# Changes in Kiwa Console 0.15

## 0.15.0 2021-06-25

### Changed

-   The console is now able to search for custom commands in the `src` directory. Also, the commands will be loaded differently, that why the `SitemapCreateCommand` has been deprecated and comes from the `kiwa/sitemap` library now.