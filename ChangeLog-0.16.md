# Changes in Kiwa Console v0.16

## 0.16.3 2022-01-10

### Fixed

-   Fixed include path.

## 0.16.2 2022-01-10

### Changed

-   Moved `bin/template-console` to root dir.

## 0.16.1 2022-01-10

### Fixed

-   Added missing line to `bin/template-console`.

## 0.16.0 2021-07-19

### Added

-   The new command `translation:clear` finds unneeded entries in translation files and removes them.
-   The new command `translation:find-missing` looks for texts that have not been translated yet. It can use the Google Translate API to translate them automatically.