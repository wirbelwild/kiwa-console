# Changes in Kiwa Console 0.13.

## 0.13.4 2021-02-10

### Fixed

-   Fixed missing handling of `og:image`.

## 0.13.3 2021-01-22

### Fixed

-   Fixed error that could result in a notice when files didn't exist.

## 0.13.2 2021-01-11

### Fixed

-   Removed deprecated parameter in `places2be/locales`.

## 0.13.1 2020-12-15

### Added

-   Added the possibility to define a path prefix for `.htpasswd` files via the `htpasswdPathPrefix` variable.

### Changed

-   Updated default viewport from `user-scalable=0, maximum-scale=1.0, width=device-width` to `width=device-width, initial-scale=1`.

## 0.13.0 2020-12-10 

### Added 

-   Added possibility to handle the cache control settings.