# Changes in Kiwa Console v0.18

## 0.18.2 2023-08-28

### Changed

-   The `translation:find-missing` command searches in path `/src` now too and looks for `php` and `phtml` files. There is also the possibility to search in a given path.

## 0.18.1 2022-03-21

### Changed

-   Updated version number for `kiwa/core`.

## 0.18.0 2022-02-28

### Changed

-   PHP >=7.4 is required.