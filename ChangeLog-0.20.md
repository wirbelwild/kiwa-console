# Changes in Kiwa Console v0.20

## 0.20.3 2023-08-28

### Changed

-   The `translation:find-missing` command searches in path `/src` now too and looks for `php` and `phtml` files. There is also the possibility to search in a given path.

## 0.20.2 2022-09-22

### Fixed

-   Updating the config file will correctly append `setDefaultLanguageCode` now.
-   A cache lifetime value of `-1` will result in an empty method call now. 

## 0.20.1 2022-09-22

### Fixed

-   Don't show full path when autoloading doesn't work for security reasons.

## 0.20.0 2022-05-12

### Changed

-   Updated dependencies.