# Changes in Kiwa Console v0.22

## 0.22.0 2023-10-20

### Changed

-   When creating a new page with a dynamic subpage, the subpage will be added to the config, too.
-   When listing all missing translations, the table will only show one line per text to keep to table more compact.