<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

use BitAndBlack\Composer\VendorPath;
use Kiwa\TemplateConsole\DynamicCommandLoader;
use PackageVersions\Versions;
use Spatie\Emoji\Emoji;
use Symfony\Component\Console\Application;
use Symfony\Component\Dotenv\Dotenv;

/**
 * @param string $file
 * @return mixed
 */
function includeIfExists(string $file)
{
    if (file_exists($file)) {
        return include $file;
    }

    return false;
}

if (!includeIfExists(__DIR__ . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php')
    && !includeIfExists(dirname(__DIR__, 3) . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php')
) {
    $message = 'You must set up the project dependencies at first. Please run the following commands:' . PHP_EOL .
        'curl -sS https://getcomposer.org/installer | php' . PHP_EOL .
        'php composer.phar install' . PHP_EOL
    ;
    fwrite(STDERR, $message);
    exit(1);
}

$dotenv = new Dotenv();
$envFile = dirname(new VendorPath()) . DIRECTORY_SEPARATOR . '.env';

if (file_exists($envFile)) {
    $dotenv->loadEnv($envFile);
}

$versionString = Versions::getVersion('kiwa/console');
$versionParts = explode('@', $versionString);
$version = $versionParts[0] ?? 'Unknown Version';

$application = new Application('Kiwa Console ' . Emoji::highVoltage() . ' ', $version);

$dynamicCommandLoader = new DynamicCommandLoader($application);
$dynamicCommandLoader->loadCommands();

$application->run();
