<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Writers;

use BitAndBlack\Helpers\ArrayHelper;
use Kiwa\Config\Generator\CacheControlGenerator;
use Kiwa\Config\Generator\ConfigFile;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Config\Generator\Variable;
use Kiwa\Exception\Config\MethodNotAvailableException;
use Kiwa\Path;
use ReflectionClass;
use ReflectionException;
use RuntimeException;
use Symfony\Component\Yaml\Yaml;

/**
 * Handling the config
 *
 * @package Kiwa\TemplateConsole\Writers
 * @internal
 * @see \Kiwa\TemplateConsole\Tests\Writers\ConfigWriterTest
 */
class ConfigWriter
{
    private ?string $outputFormat = null;

    /**
     * @var string
     */
    public const FORMAT_YAML = 'yaml';

    /**
     * @var string
     */
    public const FORMAT_PHP_METHOD = 'php-method';

    /**
     * @var string
     */
    public const FORMAT_PHP_FLAT = 'php-flat';

    /**
     * @var array{
     *     pages: array<string, array{
     *         hasDynamicChild?: string|null,
     *         name?: string,
     *         getFile?: string,
     *         pageType?: string,
     *         childOf?: string|bool,
     *         languages?: array<string, array<string, array{
     *             pageTitle: string,
     *             description: string,
     *         }>>,
     *     }>,
     *     baseMetaTags: array{
     *         robots?: string,
     *         viewport?: string,
     *         "og:site_name"?: string|null,
     *         "og:url"?: string|null,
     *         "og:title"?: array<string, string>,
     *         description?: array<string, string>,
     *         "og:description"?: array<string, string>,
     *     },
     *     viewport: string|null,
     *     robots: string|null,
     *     languageHandling?: string,
     *     languageCode?: string,
     *     htaccess?: array{
     *        useWWW: bool,
     *        useSSL: bool,
     *     },
     *     mainURL?: string,
     * }
     */
    private array $config = [
        'pages' => [],
        'baseMetaTags' => [],
        'viewport' => null,
        'robots' => null,
    ];

    /**
     * ConfigWriter constructor.
     */
    public function __construct()
    {
        $configFilePHP = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php';
        $configFileYaml = Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.yaml';
        $config = [];

        if (!file_exists(Path::getConfigFolder())
            && !mkdir($concurrentDirectory = Path::getConfigFolder())
            && !is_dir($concurrentDirectory)
        ) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
        }

        $this->setOutputFormat(self::FORMAT_PHP_METHOD);

        if (file_exists($configFilePHP)) {
            $config = include $configFilePHP;
            $content = (string) @file_get_contents($configFilePHP);

            if (!str_contains($content, 'This file has been auto-generated')) {
                $this->setOutputFormat(self::FORMAT_PHP_FLAT);
            }
        } elseif (file_exists($configFileYaml)) {
            $this->setOutputFormat(self::FORMAT_YAML);
            $config = Yaml::parseFile($configFileYaml);
        }

        if (true === is_array($config)) {
            /** @phpstan-ignore-next-line */
            $this->config = array_merge($this->config, $config);
        }
    }

    /**
     * Adds a page to the config file
     *
     * @param array{
     *     hasDynamicChild: string|bool|null,
     *     pageName: string,
     *     getFile: string,
     *     pageType: string,
     *     childOf: string|bool,
     *     languages: array<string, array{
     *         pageTitle: string|null,
     *         description: string|null,
     *     }>,
     * } $pageSettings
     * @return bool
     * @throws ReflectionException
     */
    public function addPage(array $pageSettings): bool
    {
        $config = $this->getConfig();

        foreach ($pageSettings['languages'] as $languageCode => $setting) {
            if (false === array_key_exists($languageCode, $config['pages'])) {
                $config['pages'][$languageCode] = [];
            }

            $config['pages'][$languageCode][] = [
                'name' => $pageSettings['pageName'],
                'pageTitle' => $setting['pageTitle'],
                'metaTags' => [
                    'og:title' => $setting['pageTitle'],
                    'og:description' => $setting['description'],
                    'description' => $setting['description'],
                    'og:type' => $pageSettings['pageType'],
                ],
                'getFile' => $pageSettings['getFile'],
                'hasDynamicChild' => $pageSettings['hasDynamicChild'],
                'enableCache' => false,
                'childOf' => $pageSettings['childOf'],
            ];

            if (null !== $pageSettings['hasDynamicChild']) {
                $config['pages'][$languageCode][] = [
                    'name' => $pageSettings['hasDynamicChild'],
                    'getFile' => $pageSettings['hasDynamicChild'],
                    'childOf' => $pageSettings['pageName'],
                ];
            }
        }

        return $this->writeFile($config);
    }

    /**
     * Returns all pages that can be used for nesting
     *
     * @return array<int, string>
     */
    public function getPagesLevel1(): array
    {
        /** @var array<int, string> $pagesFound */
        $pagesFound = [];

        foreach ($this->getConfig()['pages'] as $pages) {
            /**
             * @var array{
             *     name?: string,
             *     childOf?: string|null|bool,
             * } $page
             */
            foreach ($pages as $page) {
                if (!array_key_exists('childOf', $page) || null === $page['childOf'] || false === $page['childOf']) {
                    $pagesFound[] = $page['name'] ?? null;
                }
            }
        }

        $pagesFound = array_filter($pagesFound);
        return array_unique($pagesFound);
    }

    /**
     * Adds basic meta tags
     *
     * @param array{
     *     robots?: string,
     *     viewport?: string,
     *     "og:site_name"?: string|null,
     *     "og:url"?: string|null,
     *     "og:title"?: array<string, string>,
     *     description?: array<string, string>,
     *     "og:description"?: array<string, string>,
     * } $metaTags
     * @return bool
     * @throws ReflectionException
     */
    public function addMetaTags(array $metaTags): bool
    {
        $config = $this->getConfig();

        $ogUrl = $metaTags['og:url']
            ?? $config['baseMetaTags']['og:url']
            ?? ''
        ;

        $siteName = $metaTags['og:site_name']
            ?? $config['baseMetaTags']['og:site_name']
            ?? ''
        ;

        $ogTitle = array_merge(
            $config['baseMetaTags']['og:title'] ?? [],
            $metaTags['og:title'] ?? [],
        );

        $description = array_merge(
            $config['baseMetaTags']['description'] ?? [],
            $metaTags['description'] ?? [],
        );

        $ogDescription = array_merge(
            $config['baseMetaTags']['og:description'] ?? [],
            $metaTags['og:description'] ?? [],
        );

        $robots = $metaTags['robots']
            ?? $config['baseMetaTags']['robots']
            ?? 'index, follow'
        ;

        $viewport = $metaTags['viewport']
            ?? $config['baseMetaTags']['viewport']
            ?? 'width=device-width, initial-scale=1'
        ;

        $config['baseMetaTags'] = [
            'robots' => $robots,
            'viewport' => $viewport,
            'og:url' => $ogUrl,
            'og:site_name' => $siteName,
            'og:title' => $ogTitle,
            'description' => $description,
            'og:description' => $ogDescription,
        ];

        return $this->writeFile($config);
    }

    /**
     * Adds the url structure
     *
     * @param array<int, string|null> $urlStructure
     * @return bool
     * @throws ReflectionException
     */
    public function addURLStructure(array $urlStructure): bool
    {
        $config = $this->getConfig();
        $config['urlStructure'] = $urlStructure;
        return $this->writeFile($config);
    }

    /**
     * Adds the language information
     *
     * @param string $languageHandling
     * @param string $languageCode
     * @return bool
     * @throws ReflectionException
     */
    public function addLanguageInformation(string $languageHandling, string $languageCode): bool
    {
        $config = $this->getConfig();
        $config['languageHandling'] = $languageHandling;
        $config['languageCode'] = $languageCode;
        return $this->writeFile($config);
    }

    /**
     * Adds the htaccess settings
     *
     * @param array{
     *      useWWW: bool,
     *      useSSL: bool,
     *  } $htaccessSetting
     * @return bool
     * @throws ReflectionException
     */
    public function addHtaccessSetting(array $htaccessSetting): bool
    {
        $config = $this->getConfig();
        $config['htaccess'] = $htaccessSetting;
        return $this->writeFile($config);
    }

    /**
     * Returns the whole config.
     *
     * @return array{
     *     pages: array<string, array{
     *         hasDynamicChild?: string|null,
     *         name?: string,
     *         getFile?: string,
     *         pageType?: string,
     *         childOf?: string|bool,
     *         languages?: array<string, array<string, array{
     *             pageTitle: string,
     *             description: string,
     *         }>>,
     *     }>,
     *     baseMetaTags: array{
     *         robots?: string,
     *         viewport?: string,
     *         "og:site_name"?: string|null,
     *         "og:url"?: string|null,
     *         "og:title"?: array<string, string>,
     *         description?: array<string, string>,
     *         "og:description"?: array<string, string>,
     *      },
     *     viewport: string|null,
     *     robots: string|null,
     *     languageHandling?: string,
     *     languageCode?: string,
     *     htaccess?: array{
     *         useWWW: bool,
     *         useSSL: bool,
     *     },
     *     mainURL?: string,
     * }
     */
    public function getConfig(): array
    {
        return $this->config;
    }

    /**
     * Returns parts of the config file
     *
     * @param string $key
     * @return mixed
     */
    public function getValue(string $key): mixed
    {
        $config = $this->getConfig();
        return $config[$key];
    }

    /**
     * Returns if a value exists
     *
     * @param string $key
     * @return bool
     */
    public function hasValue(string $key): bool
    {
        $config = $this->getConfig();
        return isset($config[$key]);
    }

    /**
     * Writes the config file
     *
     * @param array<mixed> $config
     * @return bool
     * @throws ReflectionException
     */
    private function writeFile(array $config): bool
    {
        if (self::FORMAT_YAML === $this->outputFormat) {
            return false !== file_put_contents(
                Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.yaml',
                Yaml::dump($config, 6),
            );
        }

        /**
         * @todo Check for `@kiwa-auto-generated` in the future.
         */
        if (self::FORMAT_PHP_FLAT === $this->outputFormat) {
            return false !== file_put_contents(
                Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php',
                '<?php return ' . var_export($config, true) . ';',
            );
        }

        $file = new ConfigFile();
        $variable = $file->addVariableFromClass(ConfigGenerator::class);

        foreach ($config as $key => $value) {
            switch ($key) {
                case 'mainURL':
                    $this->addMethod($variable, 'setMainURL', $value);
                    break;
                case 'languageCode':
                    $this->addMethod($variable, 'setDefaultLanguageCode', $value);
                    break;
                case 'languageHandling':
                    $method = 'strict' === $value ? 'enableStrictLanguageHandling' : 'disableStrictLanguageHandling';
                    $this->addMethod($variable, $method, null);
                    break;
                case 'urlStructure':
                    $value = true === is_array($value) ? $value : [$value];
                    $method = 'setURLStructure';
                    $this->addMethod($variable, $method, ...$value);
                    break;
                case 'usePageSuffix':
                    $method = 'setPageSuffix';
                    $this->addMethod($variable, $method, $value);
                    break;
                case 'useHtpasswd':
                    if (!isset($config['htpasswdUser'], $config['htpasswdPassword'])) {
                        continue 2;
                    }
                    $method = 'enableAuthentication';
                    $this->addMethod($variable, $method, $config['htpasswdUser'], $config['htpasswdPassword']);
                    break;
                case 'pages':

                    $value = true === is_array($value) ? $value : [$value];

                    foreach ($value as $languageCode => $pages) {
                        foreach ($pages as $page) {
                            $variableInline = $file->addVariableFromClass(PageGenerator::class);
                            $this->addMethod($variableInline, 'setLanguageCode', $languageCode);

                            foreach ($page as $pageKey => $pageValue) {
                                switch ($pageKey) {
                                    case 'name':
                                        $method = 'setName';
                                        $this->addMethod($variableInline, $method, $pageValue);
                                        break;
                                    case 'pageTitle':
                                        $method = 'setPageTitle';
                                        $this->addMethod($variableInline, $method, $pageValue);
                                        break;
                                    case 'getFile':
                                        $method = 'setFile';
                                        $this->addMethod($variableInline, $method, $pageValue);
                                        break;
                                    case 'hasDynamicChild':
                                        if (null === $pageValue) {
                                            break;
                                        }
                                        $method = 'setDynamicChild';
                                        $this->addMethod($variableInline, $method, $pageValue);
                                        break;
                                    case 'childOf':
                                        if (false === $pageValue) {
                                            break;
                                        }
                                        $method = 'setChildOf';
                                        $this->addMethod($variableInline, $method, $pageValue);
                                        break;
                                    case 'enableCache':
                                        $cacheLifetime = $page['cacheLifetime'] ?? null;
                                        
                                        if (false === $pageValue || null === $cacheLifetime) {
                                            break;
                                        }

                                        /**
                                         * -1 stands for infinite caching and can be set without value.
                                         */
                                        if (-1 === $cacheLifetime) {
                                            $cacheLifetime = null;
                                        }

                                        $method = 'enableCaching';
                                        $this->addMethod($variableInline, $method, $cacheLifetime);
                                        break;
                                    case 'metaTags':
                                        foreach ($pageValue as $metaKey => $metaValue) {
                                            if ('og:type' === $metaKey) {
                                                $this->addMethod($variableInline, 'setPageType', $metaValue);
                                                continue;
                                            }

                                            if ('og:title' === $metaKey) {
                                                $this->addMethod($variableInline, 'setOGTitle', $metaValue);
                                                continue;
                                            }

                                            if ('description' === $metaKey) {
                                                $this->addMethod($variableInline, 'setDescription', $metaValue);
                                                continue;
                                            }

                                            if ('og:description' === $metaKey) {
                                                $this->addMethod($variableInline, 'setOGDescription', $metaValue);
                                                continue;
                                            }

                                            if ('og:image' === $metaKey) {
                                                $this->addMethod($variableInline, 'setOGImage', $metaValue);
                                                continue;
                                            }
                                        }
                                        break;
                                }
                            }

                            $reflected = new ReflectionClass(PageGenerator::class);
                            $methodChain = [$reflected->getShortName() . '::create()'];
                            array_push($methodChain, ...$variableInline->getLines(false));

                            $methodChainString = implode(
                                PHP_EOL . ConfigGenerator::$indentation,
                                $methodChain,
                            );

                            $methodChainString .= PHP_EOL . ConfigGenerator::$indentation;

                            $variable->disableVarDump();
                            $this->addMethod($variable, 'addPage', $methodChainString);
                            $variable->enableVarDump();
                        }
                    }
                    break;
                case 'htaccess':
                    $value = true === is_array($value) ? $value : [$value];

                    foreach ($value as $metaKey => $metaValue) {
                        if ('useWWW' === $metaKey) {
                            $method = $metaValue ? 'enableWWW' : 'disableWWW';
                            $this->addMethod($variable, $method, null);
                            continue;
                        }
                        if ('useSSL' === $metaKey) {
                            $method = $metaValue ? 'enableSSL' : 'disableSSL';
                            $this->addMethod($variable, $method, null);
                            continue;
                        }
                    }
                    break;
                case 'cacheControl':
                    $variableInline = $file->addVariableFromClass(CacheControlGenerator::class);
                    $value = true === is_array($value) ? $value : [$value];

                    foreach ($value as $fileType => $maxAge) {
                        switch ($fileType) {
                            case 'maxAgeAssetFiles':
                                $method = 'setMaxAgeAssetFiles';
                                $this->addMethod($variableInline, $method, $maxAge);
                                break;
                            case 'maxAgeImmutableFiles':
                                $method = 'setMaxAgeImmutableFiles';
                                $this->addMethod($variableInline, $method, $maxAge);
                                break;
                            case 'maxAgeUnknownFiles':
                                $method = 'setMaxAgeUnknownFiles';
                                $this->addMethod($variableInline, $method, $maxAge);
                                break;
                        }
                    }
                    
                    $reflected = new ReflectionClass(CacheControlGenerator::class);
                    $methodChain = [$reflected->getShortName() . '::create()'];
                    array_push($methodChain, ...$variableInline->getLines(false));

                    $methodChainString = implode(
                        PHP_EOL . ConfigGenerator::$indentation,
                        $methodChain,
                    );

                    $methodChainString .= PHP_EOL . ConfigGenerator::$indentation;

                    $variable->disableVarDump();
                    $this->addMethod($variable, 'setCacheControl', $methodChainString);
                    $variable->enableVarDump();
                    break;
                case 'baseMetaTags':
                    $value = true === is_array($value) ? $value : [$value];

                    foreach ($value as $metaKey => $metaValue) {
                        if ('robots' === $metaKey) {
                            $this->addMethod($variable, 'setDefaultRobots', $metaValue);
                            continue;
                        }

                        if ('viewport' === $metaKey) {
                            $this->addMethod($variable, 'setDefaultViewport', $metaValue);
                            continue;
                        }

                        if ('og:title' === $metaKey) {
                            foreach ($metaValue as $languageCode => $title) {
                                $this->addMethod($variable, 'setDefaultOGTitle', $languageCode, $title);
                            }
                            continue;
                        }

                        if ('description' === $metaKey) {
                            foreach ($metaValue as $languageCode => $title) {
                                $this->addMethod($variable, 'setDefaultDescription', $languageCode, $title);
                            }
                            continue;
                        }

                        if ('og:description' === $metaKey) {
                            foreach ($metaValue as $languageCode => $title) {
                                $this->addMethod($variable, 'setDefaultOGDescription', $languageCode, $title);
                            }
                            continue;
                        }

                        if ('og:image' === $metaKey) {
                            $this->addMethod($variable, 'setDefaultOGImage', $metaValue);
                            continue;
                        }
                    }
                    continue 2;
                default:
                    continue 2;
            }
        }

        $file
            ->addContentFragment($variable)
            ->addReturn(
                $variable,
                'getConfig',
            )
        ;

        return false !== file_put_contents(
            Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php',
            $file,
        );
    }

    /**
     * This method helps adding a string-method.
     *
     * @param Variable $variable
     * @param string $methodName
     * @return $this
     */
    private function addMethod(Variable $variable, string $methodName, mixed ...$parameter): self
    {
        try {
            $variable->addChainedMethodCall(
                $methodName,
                ...ArrayHelper::getArray($parameter),
            );
        } catch (MethodNotAvailableException) {
        }

        return $this;
    }

    /**
     * Sets the output format for the config file.
     *
     * @param string $format
     * @return $this
     */
    public function setOutputFormat(string $format): self
    {
        $this->outputFormat = $format;
        return $this;
    }

    /**
     * Writes the config file from new.
     *
     * @return bool
     * @throws ReflectionException
     */
    public function rewriteConfig(): bool
    {
        $config = $this->getConfig();
        return $this->writeFile($config);
    }
}
