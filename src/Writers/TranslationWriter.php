<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Writers;

use Kiwa\Path;
use RuntimeException;

/**
 * Handling translations
 *
 * @internal
 * @package Kiwa\TemplateConsole\Writers
 */
class TranslationWriter
{
    /**
     * Saves translations to the file system.
     *
     * @param array<string, array<string, string>> $translations An array of multiple languages and their translations.
     *                                                           The structure is like that:
     *
     *                                                           ```
     *                                                           [
     *                                                               'de-de' => [
     *                                                                   'My translation' => 'Meine Übersetzung'
     *                                                               ]
     *                                                           ]
     *                                                           ```
     * @return bool
     */
    public function saveTranslations(array $translations): bool
    {
        $translationsDir = Path::getLanguageFolder();
        $success = true;
        
        if (!file_exists($translationsDir) && !mkdir($translationsDir) && !is_dir($translationsDir)) {
            throw new RuntimeException(
                sprintf('Directory "%s" was not created', $translationsDir)
            );
        }

        foreach ($translations as $messageDomain => $translation) {
            $success &= false !== file_put_contents(
                $translationsDir . DIRECTORY_SEPARATOR . $messageDomain . '.php',
                '<?php return ' . var_export($translation, true) . ';' . PHP_EOL
            );
        }

        return (bool) $success;
    }
}
