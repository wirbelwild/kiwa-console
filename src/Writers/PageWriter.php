<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Writers;

use BitAndBlack\Composer\VendorPath;
use RuntimeException;

/**
 * Handling pages
 *
 * @internal
 * @package Kiwa\TemplateConsole\Writers
 */
class PageWriter
{
    /**
     * @var string
     */
    private $pageTemplate =
        '<style>' . PHP_EOL .
        '    body {' . PHP_EOL .
        '        font-family: sans-serif;' . PHP_EOL .
        '        background-color: lightgray;' . PHP_EOL .
        '        padding: 2rem;' . PHP_EOL .
        '    }' . PHP_EOL .
        '</style>' . PHP_EOL .
        '<section>' . PHP_EOL .
        '    <h1>Welcome to page <code>%1$s</code>!</h1>' . PHP_EOL .
        '    <p>This file is located under <code>%2$s</code>.</p>' . PHP_EOL .
        '</section>'
    ;
    
    /**
     * Adds a phtml page file
     *
     * @param string $pageName
     * @return bool
     */
    public function addPage(string $pageName): bool
    {
        $root = dirname((string) new VendorPath());
        
        $pageFolder = $root . DIRECTORY_SEPARATOR . 'html';
        $pageFile = $pageFolder . DIRECTORY_SEPARATOR . $pageName . '.phtml';
        
        if (!file_exists($pageFolder) && !mkdir($pageFolder) && !is_dir($pageFolder)) {
            throw new RuntimeException(
                sprintf('Directory "%s" was not created', $pageFolder)
            );
        }

        if (!file_exists($pageFile)) {
            return false !== file_put_contents(
                $pageFile,
                sprintf($this->pageTemplate, $pageName, $pageFile)
            );
        }

        return true;
    }
}
