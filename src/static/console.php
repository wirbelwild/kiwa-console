<?php exit(0); ?>
#!/usr/bin/env php
<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

use BitAndBlack\Composer\Composer;
use Kiwa\TemplateConsole\Environment;

$vendor = implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'vendor']);
$autoloadFile = implode(DIRECTORY_SEPARATOR, [$vendor, 'autoload.php']);
$consoleFile = implode(DIRECTORY_SEPARATOR, [$vendor, 'kiwa', 'console', 'kiwa-console.php']);

if (!file_exists($autoloadFile)) {
    echo 'Unable to find "' . $autoloadFile . '".' . PHP_EOL;
    echo 'Maybe you need to install composers dependencies at first: "$ composer install".' . PHP_EOL;
    exit(0);
}

if (!file_exists($consoleFile)) {
    echo 'Unable to find "' . $consoleFile . '".' . PHP_EOL;
    echo 'Maybe you should try to reinstall the console module: "$ composer require kiwa/console".' . PHP_EOL;
    exit(0);
}

require_once $autoloadFile;

if (Composer::classExists(Environment::class)) {
    Environment::runsFromTemplate();
}

require_once $consoleFile;
