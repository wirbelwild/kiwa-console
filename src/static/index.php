<?php exit(0); ?>
<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

use Kiwa\Frontend\Controller;

$autoload = implode(DIRECTORY_SEPARATOR, [dirname(__DIR__), 'vendor', 'autoload.php']);

if (!file_exists($autoload)) {
    echo 'Unable to find "vendor/autoload.php".' . PHP_EOL;
    echo 'Maybe you should try to install your dependencies by running "$ composer install".' . PHP_EOL;
    exit(1);
}

require_once $autoload;

$controller = new Controller();
$controller->printResponse();
