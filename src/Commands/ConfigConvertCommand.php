<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use BitAndBlack\SentenceConstruction;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConfigConvertCommand.
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class ConfigConvertCommand extends Command
{
    /**
     * @var string[]
     */
    private array $allowedFormats = [
        ConfigWriter::FORMAT_YAML,
        ConfigWriter::FORMAT_PHP_METHOD,
        ConfigWriter::FORMAT_PHP_FLAT,
    ];

    /**
     * RobotsTxtCreateCommand constructor.
     *
     * @param ConfigWriter $configWriter
     */
    public function __construct(
        private readonly ConfigWriter $configWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('config:convert')
            ->setDescription('Converts the config file into another format.')
            ->setHelp(
                'This command converts the config file into another format:' . PHP_EOL .
                '<info>' . ConfigWriter::FORMAT_YAML . '</info> will create a <info>.yaml</info> file,' . PHP_EOL .
                '<info>' . ConfigWriter::FORMAT_PHP_METHOD . '</info> will create a <info>.php</info> file containing setter methods,' . PHP_EOL .
                '<info>' . ConfigWriter::FORMAT_PHP_FLAT . '</info> will create a <info>.php</info> file with an array inside.'
            )
            ->addArgument(
                'format',
                InputArgument::REQUIRED,
                'The format: can be <info>' .
                    new SentenceConstruction('</info>, <info>', '</info> or <info>', $this->allowedFormats) . '</info>.'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ReflectionException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $format = $input->getArgument('format');
        
        if (!in_array($format, $this->allowedFormats, true)) {
            $output->writeln('<error>Wrong format.</error>');
            return Command::FAILURE;
        }
        
        $this->configWriter->setOutputFormat($format);
        $success = $this->configWriter->rewriteConfig();
        
        if (!$success) {
            $output->writeln('<info>Error while creating the new config file.</info>');
            return Command::FAILURE;
        }

        $output->writeln(
            '<info>Successfully created a new config file.</info>' . PHP_EOL .
            'Don\'t forget to remove the old file.'
        );

        return Command::SUCCESS;
    }
}
