<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use BitAndBlack\Helpers\StringHelper;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\ExceptionInterface;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Class PathStructureCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class PathStructureCommand extends Command
{
    /**
     * PathStructureCommand constructor.
     *
     * @param ConfigWriter $configWriter
     */
    public function __construct(
        private readonly ConfigWriter $configWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('url:define')
            ->setDescription('Defines the url structure.')
            ->setHelp('This command allows you to define the url structure including SSL, www and more.')
        ;
    }

    /**
     * Helps to define the url structure
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ReflectionException
     * @throws ExceptionInterface
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $urlStructure = [];

        $parametersAvailable = [
            '0' => 'The language code',
            '1' => 'First parameter (name)',
            '2' => 'Second parameter (subname)',
        ];

        $parametersNames = [
            '0' => 'languageCode',
            '1' => 'name',
            '2' => 'subname',
            '3' => null,
        ];

        $output->writeln('You can define the position of three url elements now.' . PHP_EOL .
            'Think about a url like <info>https://www.domain.com/en-us/about/contact.html</info>' . PHP_EOL .
            '<info>en-us</info> is your first parameter called <info>language code</info>, <info>about</info> the second one called <info>name</info>, <info>contact</info> the third one called <info>subname</info>.');

        $parameter = null;

        while (null === $parameter) {
            $question = new ChoiceQuestion(
                'Which part should be at position 1? (Defaults to <info>' . $parametersAvailable[key($parametersAvailable)] . '</info>): ',
                $parametersAvailable,
                $parametersAvailable[key($parametersAvailable)]
            );

            $question->setErrorMessage('Type %s is invalid.');
            $parameter = $helper->ask($input, $output, $question);
        }

        if (false !== ($key = array_search($parameter, $parametersAvailable, true))) {
            $urlStructure[0] = $parametersNames[$key];
            unset($parametersAvailable[$key]);
        }

        $parameter = null;

        while (null === $parameter) {
            $question = new ChoiceQuestion(
                'Which part should be at position 2? (Defaults to <info>' . ($parametersAvailable[array_key_first($parametersAvailable)] ?? null) . '</info>): ',
                $parametersAvailable,
                $parametersAvailable[array_key_first($parametersAvailable)] ?? null
            );

            $question->setErrorMessage('Type %s is invalid.');
            $parameter = $helper->ask($input, $output, $question);
        }

        if (false !== ($key = array_search($parameter, $parametersAvailable, true))) {
            $urlStructure[1] = $parametersNames[$key];
            unset($parametersAvailable[$key]);
        }

        $parameter = null;

        while (null === $parameter) {
            $question = new ChoiceQuestion(
                'Which part should be at position 3? (Defaults to <info>' . ($parametersAvailable[array_key_first($parametersAvailable)] ?? null) . '</info>): ',
                [...$parametersAvailable, ...['-- Nothing --']],
                $parametersAvailable[array_key_first($parametersAvailable)] ?? null
            );

            $question->setErrorMessage('Type %s is invalid.');
            $parameter = $helper->ask($input, $output, $question);
        }

        if (false !== ($key = array_search($parameter, $parametersAvailable, true))) {
            $urlStructure[2] = $parametersNames[$key];
            unset($parametersAvailable[$key]);
        }

        if (!$this->configWriter->addURLStructure($urlStructure)) {
            $output->writeln('<error>Failed adding url structure</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully added url structure</info>');

        $output->writeln('Now let\'s talk about SSL and www.');

        /**
         * @var array{
         *     useWWW: bool,
         *     useSSL: bool,
         * } $htaccessSettings
         */
        $htaccessSettings = [];

        $question = new ChoiceQuestion(
            'Do you want to have your url with <info>www</info>? (Defaults to <info>Yes, with www</info>): ',
            [
                'yes' => 'Write URL with "www"',
                'no' => 'Write URL without "www"',
            ],
            'yes'
        );

        $question->setErrorMessage('Type %s is invalid.');

        /** @var string $useWWW */
        $useWWW = $helper->ask($input, $output, $question);

        $htaccessSettings['useWWW'] = (bool) StringHelper::stringToBooleanAdvanced($useWWW);

        $question = new ChoiceQuestion(
            'Do you want to use SSL so your page starts with <info>https</info>? (Defaults to <info>Yes, use SSL</info>): ',
            [
                'yes' => 'Yes, use https',
                'no' => 'No, use http',
            ],
            'yes'
        );

        $question->setErrorMessage('Type %s is invalid.');

        /** @var string $useSSL */
        $useSSL = $helper->ask($input, $output, $question);

        $htaccessSettings['useSSL'] = (bool) StringHelper::stringToBooleanAdvanced($useSSL);

        if (!$this->configWriter->addHtaccessSetting($htaccessSettings)) {
            $output->writeln('<error>Failed adding .htaccess settings</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully added .htacces settings</info>');

        $question = new Question(
            'The .htaccess file is no longer up to date. Do you want to recreate it? (Press enter for <comment>Yes</comment>, else type <info>No</info>): ',
            true
        );
        
        $update = $helper->ask($input, $output, $question);
        
        if (true === $update && null !== $command = $this->getApplication()) {
            $updateCommand = $command->find('htaccess:create');
            return $updateCommand->run($input, $output);
        }
        
        return Command::SUCCESS;
    }
}
