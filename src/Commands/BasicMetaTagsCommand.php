<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use JsonException;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use Places2Be\Locales\Exception as Places2BeException;
use Places2Be\Locales\LanguageCode;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

/**
 * Class BasicMetaTagsCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class BasicMetaTagsCommand extends Command
{
    /**
     * BasicMetaTagsCommand constructor.
     *
     * @param ConfigWriter $configWriter
     */
    public function __construct(
        private readonly ConfigWriter $configWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('basic:metatags')
            ->setDescription('Defines basic meta tags.')
            ->setHelp('This command allows you to define some basic meta tags.')
        ;
    }

    /**
     * Helps to create meta-tags
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ReflectionException
     * @throws JsonException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        LanguageCode::allowCountryUnspecificLanguageCode();

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        /**
         * @var array{
         *     robots?: string,
         *     viewport?: string,
         *     "og:site_name"?: string|null,
         *     "og:url"?: string|null,
         *     "og:title"?: array<string, string>,
         *     description?: array<string, string>,
         *     "og:description"?: array<string, string>,
         * } $metaTags
         */
        $metaTags = [];

        $question = new Question('Please enter the main <info>url</info> (or press <comment>enter</comment> to continue): ', null);

        /** @var string|null $url */
        $url = $helper->ask($input, $output, $question);

        $metaTags['og:url'] = null !== $url ? mb_strtolower((string) $url) : null;

        $question = new Question('Please enter the default <info>site name</info> (or press <comment>enter</comment> to continue): ', null);

        /** @var string|null $siteName */
        $siteName = $helper->ask($input, $output, $question);
        $metaTags['og:site_name'] = $siteName;

        $addMoreTitles = true;

        while (false !== $addMoreTitles) {
            $question = new Question('Please enter a default <info>title</info> of your page in a random language (or press <comment>enter</comment> to continue): ');

            /** @var string|null $title */
            $title = $helper->ask($input, $output, $question);

            if (null === $title) {
                break;
            }

            $languageCode = null;

            while (null === $languageCode) {
                $question = new Question('Please enter the <info>language code</info> for that title: ');
                $languageCode = $helper->ask($input, $output, $question);

                if (false === is_string($languageCode)) {
                    $output->writeln('Sorry, the language code is required');
                    $languageCode = null;
                    continue;
                }

                try {
                    $languageCodeValid = new LanguageCode($languageCode);
                } catch (Places2BeException) {
                    $output->writeln('This language code is not valid. Please try again.');
                    $languageCode = null;
                    continue;
                }

                $languageCode = $languageCodeValid->getLanguageCode();
            }

            $metaTags['og:title'][$languageCode] = $title;

            $question = new Question('Do you want to add a title in another language? Enter <info>yes</info> or press <comment>enter</comment> to abort. ', false);
            $addMoreTitles = $helper->ask($input, $output, $question);
        }

        $addMoreDescriptions = true;

        while (false !== $addMoreDescriptions) {
            $question = new Question('Please enter a default <info>description</info> of your page in a random language (or press <comment>enter</comment> to continue): ');

            /** @var string|null $description */
            $description = $helper->ask($input, $output, $question);

            if (null === $description) {
                break;
            }

            $languageCode = null;

            while (null === $languageCode) {
                $question = new Question('Please enter the <info>language code</info> for that description: ');

                /** @var string|null $languageCode */
                $languageCode = $helper->ask($input, $output, $question);

                if (false === is_string($languageCode)) {
                    $output->writeln('Sorry, the language code is required');
                    $languageCode = null;
                    continue;
                }

                try {
                    $languageCodeValid = new LanguageCode($languageCode);
                } catch (Places2BeException) {
                    $output->writeln('This language code is not valid. Please try again.');
                    $languageCode = null;
                    continue;
                }

                $languageCode = $languageCodeValid->getLanguageCode();
            }

            $metaTags['description'][$languageCode] = $description;
            $metaTags['og:description'][$languageCode] = $description;

            $question = new Question('Do you want to add a description in another language? Enter <info>yes</info> or press <comment>enter</comment> to abort. ', false);
            $addMoreDescriptions = $helper->ask($input, $output, $question);
        }
        
        if (!$this->configWriter->addMetaTags($metaTags)) {
            $output->writeln('<error>Failed add meta information.</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully added meta information.</info>');
        return Command::SUCCESS;
    }
}
