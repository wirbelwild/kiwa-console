<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use Kiwa\Path;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MetaStormFileCreateCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setName('metastorm:file:create')
            ->setDescription('Creates or updates a <info>.meta-storm.xml</info> file in the project root.')
            ->setHelp('This command creates or updates a <info>.meta-storm.xml</info> file in the project root. It can be used together with the MetaStorm plugin for IntelliJ IDEA Ultimate and PhpStorm (https://plugins.jetbrains.com/plugin/26121-metastorm) and provides some autocompletion.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $fileName = '.meta-storm.xml';

        $sourcePath = dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . $fileName;
        $destinationPath = Path::getRootFolder() . DIRECTORY_SEPARATOR . $fileName;

        $success = copy($sourcePath, $destinationPath);

        if (false === $success) {
            $output->writeln('<error>Failed to copy file.</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully copied file.</info>');
        return Command::SUCCESS;
    }
}
