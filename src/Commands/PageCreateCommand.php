<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use JsonException;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use Kiwa\TemplateConsole\Writers\PageWriter;
use Places2Be\Locales\Exception as Places2BeException;
use Places2Be\Locales\LanguageCode;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Class PageCreateCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class PageCreateCommand extends Command
{
    /**
     * PageCreateCommand constructor.
     *
     * @param ConfigWriter $configWriter
     * @param PageWriter $pageWriter
     */
    public function __construct(
        private readonly ConfigWriter $configWriter,
        private readonly PageWriter $pageWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('page:create')
            ->setDescription('Creates a new page.')
            ->setHelp('This command allows you to create a page.')
        ;
    }

    /**
     * Helps to create a new page.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ReflectionException
     * @throws JsonException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        LanguageCode::allowCountryUnspecificLanguageCode();

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        /**
         * @var array{
         *     hasDynamicChild: null,
         *     languages: array<string, array{
         *         pageTitle: string|null,
         *         description: string|null,
         *     }>,
         *     pageName?: string|null,
         *     getFile: string,
         *     pageType: string,
         *     childOf: string|bool,
         *     hasDynamicChild?: bool,
         * } $pageSettings
         */
        $pageSettings = [
            'hasDynamicChild' => null,
            'languages' => [],
        ];
        
        $pageName = null;
        
        while (null === $pageName) {
            $question = new Question('Please enter the name of the page (will be part of the URL): ');

            /** @var string|null $pageName */
            $pageName = $helper->ask($input, $output, $question);

            if (null === $pageName) {
                $output->writeln('Sorry, the page name is required');
            }
        }
        
        $pageSettings['pageName'] = mb_strtolower((string) $pageName);

        $question = new Question('Do you want to root to a file with the same name? If yes press <comment>enter</comment>, else enter the <info>filename</info>, please: ', true);

        /** @var string|bool $fileName */
        $fileName = $helper->ask($input, $output, $question);
        $fileNameModified = mb_strtolower((string) $fileName);
        $fileNameModified = str_replace([' ', '_'], ['-', '-'], $fileNameModified);

        $pageSettings['getFile'] = $fileNameModified;

        if (true === $fileName) {
            $pageSettings['getFile'] = mb_strtolower((string) $pageName);
        }

        $question = new ChoiceQuestion(
            'Please select the page type (defaults to <info>website</info>): ',
            ['website', 'article', 'book', 'profile'],
            'website'
        );
        
        $question->setErrorMessage('Type %s is invalid.');

        /** @var string $pageType */
        $pageType = $helper->ask($input, $output, $question);

        $pageSettings['pageType'] = $pageType;

        $question = new Question(
            'Is this page a child of another page? If no press <comment>enter</comment>, else enter the <info>name</info> of that page, please: ',
            false
        );
        
        $question->setAutocompleterValues($this->configWriter->getPagesLevel1());

        /** @var string|false $childOf */
        $childOf = $helper->ask($input, $output, $question);

        $pageSettings['childOf'] = $childOf;

        if (false === $pageSettings['childOf']) {
            $question = new Question(
                'Does this page allows dynamic children pages, maybe for a blog? If no press <comment>enter</comment>, else tell us the <info>name</info> of the file which you want to load for these requests: ',
                false
            );

            /** @var string|bool $hasDynamicChild */
            $hasDynamicChild = $helper->ask($input, $output, $question);

            if (true === is_string($hasDynamicChild)) {
                $pageSettings['hasDynamicChild'] = $hasDynamicChild;
            }
        }

        $addMoreLanguages = true;

        while (false !== $addMoreLanguages) {
            $question = new Question('Please enter the <info>language code</info> of that page: ');
            $languageCode = $helper->ask($input, $output, $question);
            
            if (null === $languageCode && !empty($pageSettings['languages'])) {
                break;
            }

            if (false === is_string($languageCode)) {
                $output->writeln('Sorry, the language code is required');
                $languageCode = null;
                continue;
            }

            try {
                $languageCodeValid = new LanguageCode($languageCode);
            } catch (Places2BeException) {
                $output->writeln('This language code is not valid. Please try again.');
                $languageCode = null;
                continue;
            }

            $languageCode = $languageCodeValid->getLanguageCode();

            $question = new Question('Please enter the <info>title</info> for language "' . $languageCode . '": ', null);

            /** @var string|null $pageTitle */
            $pageTitle = $helper->ask($input, $output, $question);

            $pageSettings['languages'][$languageCode]['pageTitle'] = $pageTitle;
    
            $question = new Question('Please enter the <info>description</info> for language "' . $languageCode . '": ', null);

            /** @var string|null $description */
            $description = $helper->ask($input, $output, $question);

            $pageSettings['languages'][$languageCode]['description'] = $description;
            
            $question = new Question('Do you want to add another language? Enter <info>yes</info> or press <comment>enter</comment> to abort. ', false);
            $addMoreLanguages = $helper->ask($input, $output, $question);
        }
        
        if (!$this->configWriter->addPage($pageSettings)) {
            $output->writeln('Failed to create config for page <info>' . $pageSettings['pageName'] . '</info>');
            return Command::FAILURE;
        }

        if (!$this->pageWriter->addPage($pageSettings['getFile'])) {
            $output->writeln('Failed to created page <info>' . $pageSettings['getFile'] . '</info>');
            return Command::FAILURE;
        }

        $dynamicChild = $pageSettings['hasDynamicChild'];

        if (true === is_string($dynamicChild) && false === $this->pageWriter->addPage($dynamicChild)) {
            $output->writeln('Failed to created page <info>' . $dynamicChild . '</info>');
            return Command::FAILURE;
        }

        $output->writeln('Successfully created page <info>' . $pageSettings['pageName'] . '</info>');
        return Command::SUCCESS;
    }
}
