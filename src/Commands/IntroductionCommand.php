<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use Kiwa\TemplateConsole\Environment;
use Spatie\Emoji\Emoji;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class IntroductionCommand
 *
 * @package Kiwa\Command
 */
class IntroductionCommand extends Command
{
    private string $consolePath;

    /**
     * IntroductionCommand constructor.
     */
    public function __construct()
    {
        $this->consolePath = 'vendor/bin/kiwa-template';
        
        if (Environment::hasBeenCalledFromTemplate()) {
            $this->consolePath = 'bin/console';
        }

        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('introduction')
            ->setDescription('Shows the introduction.')
            ->setHelp('This command shows the introduction which is called after the first installation.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('');
        $output->writeln('<fg=black;bg=green>                </>');
        $output->writeln('<fg=black;bg=green>  Kiwa Console  </>');
        $output->writeln('<fg=black;bg=green>                </>');
        $output->writeln('');
        $output->writeln('<info>Welcome to Kiwa ' . Emoji::highVoltage() . '</info>. The console module allows an easier handling of your template. You can run the CLI whenever you want by calling <comment>$ php ' . $this->consolePath . '</comment>.');
        $output->writeln('');
        $output->writeln('Next steps:');
        $output->writeln('  1. Move to your project directory.');
        $output->writeln('  2. Create your code repository, for example by running <comment>$ git init</comment>.');
        $output->writeln('  3. To install all dependencies from Node, run <comment>$ yarn install</comment> or <comment>$ npm install</comment>.');
        $output->writeln('');
        return Command::SUCCESS;
    }
}
