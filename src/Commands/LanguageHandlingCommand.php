<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use BitAndBlack\SentenceConstruction;
use JsonException;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use Places2Be\Locales\Exception;
use Places2Be\Locales\LanguageCode;
use ReflectionException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Class LanguageHandlingCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class LanguageHandlingCommand extends Command
{
    /**
     * LanguageHandlingCommand constructor.
     *
     * @param ConfigWriter $configWriter
     */
    public function __construct(
        private readonly ConfigWriter $configWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('language:define')
            ->setDescription('Defines the language handling.')
            ->setHelp('This command allows you to define the handling which is important for multilingual pages.')
        ;
    }

    /**
     * Helps to define the url structure
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ReflectionException
     * @throws JsonException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        LanguageCode::allowCountryUnspecificLanguageCode();

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');
        
        $parametersAvailable = [
            '0' => 'No',
            '1' => 'Yes',
        ];
        
        $parametersNames = [
            '0' => 'strict',
            '1' => 'flexible',
        ];
        
        $parameter = null;

        while (null === $parameter) {
            $question = new ChoiceQuestion(
                'Do you want to allow visitors to jump between different language versions? (Defaults to <info>' . $parametersAvailable[key($parametersAvailable)] . '</info>): ',
                $parametersAvailable,
                $parametersAvailable[key($parametersAvailable)]
            );

            $question->setErrorMessage('Type %s is invalid.');
            $parameter = $helper->ask($input, $output, $question);
        }

        $languageHandling = 'strict';
        
        if (false !== $key = array_search($parameter, $parametersAvailable, true)) {
            $languageHandling = $parametersNames[$key];
        }
        
        $pagesExisting = $this->configWriter->getConfig()['pages'];

        /** @var array<int, string> $languagesFromConfig */
        $languagesFromConfig = array_keys($pagesExisting);
        
        $parameter = null;
        $languageCode = null;

        $suggestion = 'There are no language information in your config, so please enter a language code by your own: ';
        
        if (!empty($languagesFromConfig)) {
            $suggestion = 'We found <info>' . new SentenceConstruction('</info>, <info>', '</info> and <info>', $languagesFromConfig) . '</info>, but it may be something else: ';
        }
        
        while (null === $parameter) {
            $question = new Question(
                'Please enter a default language code. ' . $suggestion,
                ''
            );
            
            $question->setAutocompleterValues($languagesFromConfig);

            $parameter = $helper->ask($input, $output, $question);

            if (false === is_string($parameter)) {
                $parameter = null;
            }

            try {
                $languageCode = new LanguageCode((string) $parameter);
            } catch (Exception) {
                $output->writeln('Please enter a valid language code');
                $parameter = null;
            }
        }
        
        if (!$this->configWriter->addLanguageInformation($languageHandling, (string) $languageCode)) {
            $output->writeln('<error>Failed adding language information</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully added language information</info>');
        return Command::SUCCESS;
    }
}
