<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use BitAndBlack\Composer\VendorPath;
use JsonException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ComposerJsonUpdateCommand.
 * @see \Kiwa\TemplateConsole\Tests\Commands\ComposerJsonUpdateCommandTest
 */
class ComposerJsonUpdateCommand extends Command
{
    /**
     * @var array<string,
     *     array<string, string|array<int|string, string>>
     * >
     */
    private array $config = [
        'scripts' => [
            'post-install-cmd' => [
                '@kiwa-introduction',
            ],
            'post-package-install' => 'Kiwa\\ComposerHandler::postPackageInstall',
            'post-package-uninstall' => 'Kiwa\\ComposerHandler::postPackageUninstall',
            'kiwa-introduction' => [
                'php bin/console introduction --ansi',
            ],
        ],
        'scripts-descriptions' => [
            'kiwa-introduction' => 'Shows the initial introduction message which will be show after the first installation of Kiwa (www.kiwa.io). See more under <info>$ php bin/console introduction --help</info>.',
        ],
    ];
    
    private readonly string $composerJsonFile;

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('composer:json:update')
            ->setDescription('Updates the composer.json file.')
            ->setHelp('This command updates the composer.json file by adding some scripts for Kiwa.')
        ;
    }

    public function __construct()
    {
        $path = dirname(new VendorPath());
        $this->composerJsonFile = $path . DIRECTORY_SEPARATOR . 'composer.json';
        parent::__construct();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        if (!file_exists($this->composerJsonFile)) {
            $output->writeln('<error>No composer.json file found.</error>');
            return Command::FAILURE;
        }
        
        $composerJsonContentString = $this->getComposerJsonContent();
        
        try {
            /** @var array<string, array<string, string|array<int|string, string>>> $composerJsonContent */
            $composerJsonContent = json_decode($composerJsonContentString, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException) {
            $output->writeln('<error>Error reading composer.json file.</error>');
            return Command::FAILURE;
        }

        $composerJsonContent = $this->mergeConfiguration($composerJsonContent, $this->config);
        
        $success = false !== $this->setComposerJsonContent($composerJsonContent);

        if (!$success) {
            $output->writeln('<error>Error while writing composer.json file</error>');
            return Command::FAILURE;
        }
        
        $output->writeln('<info>Successfully updated composer.json file</info>');
        return Command::SUCCESS;
    }

    /**
     * @return string
     */
    protected function getComposerJsonContent(): string
    {
        return (string) file_get_contents($this->composerJsonFile);
    }

    /**
     * @param array<string, array<string, string|array<int|string, string>>> $composerJsonContent
     * @return bool
     */
    protected function setComposerJsonContent(array $composerJsonContent): bool
    {
        try {
            $composerJsonContentString = json_encode(
                $composerJsonContent,
                JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
            );
        } catch (JsonException) {
            return false;
        }

        if (!is_string($composerJsonContentString)) {
            return false;
        }
        
        return false !== file_put_contents($this->composerJsonFile, $composerJsonContentString);
    }

    /**
     * @param array<string, array<string, string|array<int|string, string>>> $configExisting
     * @param array<string, array<string, string|array<int|string, string>>> $configNew
     * @return array<string, array<string, string|array<int|string, string>>>
     */
    private function mergeConfiguration(array $configExisting, array $configNew): array
    {
        foreach ($configNew as $configKey => $configValues) {
            if (!is_array($configValues)) {
                $configExisting[$configKey] = $configValues;
                continue;
            }
            
            foreach ($configValues as $configKeyNested => $configValuesNested) {
                if (!is_array($configValuesNested)) {
                    $configExisting[$configKey][$configKeyNested] = $configValuesNested;
                    continue;
                }

                if (is_array($configExisting[$configKey][$configKeyNested])) {
                    $configExisting[$configKey][$configKeyNested] = array_merge(
                        $configExisting[$configKey][$configKeyNested],
                        $configValuesNested
                    );

                    $configExisting[$configKey][$configKeyNested] = array_unique($configExisting[$configKey][$configKeyNested]);
                }
            }
        }
        
        return $configExisting;
    }
}
