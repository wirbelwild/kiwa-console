<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use BitAndBlack\PathInfo\PathInfo;
use Kiwa\Language\Text;
use Kiwa\Path;
use Kiwa\TemplateConsole\FileFinder;
use Kiwa\TemplateConsole\NodeVisitor;
use Kiwa\TemplateConsole\Writers\TranslationWriter;
use PhpParser\Error;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\ParserFactory;
use PhpParser\PhpVersion;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @see \Kiwa\TemplateConsole\Tests\Commands\TranslationUnusedRemoveCommandTest
 */
class TranslationUnusedRemoveCommand extends Command
{
    /**
     * TranslationFindMissingCommand constructor.
     *
     * @param TranslationWriter $translationWriter
     */
    public function __construct(
        private readonly TranslationWriter $translationWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('translation:unused:remove')
            ->setDescription('Removes unused translations.')
            ->setHelp(
                'This command finds unused translations in your language files.' . PHP_EOL .
                'This means, Kiwa searches for texts that have been echoed with the ' .
                '<comment>Kiwa\Language\Text</comment> class and compares them with the language files.'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $regex = '#^.*\.(phtml|php)$#';

        $files = FileFinder::findPhtmlFiles(
            Path::getRootFolder(),
        );

        $parserFactory = new ParserFactory();
        $parser = $parserFactory->createForVersion(PhpVersion::getHostVersion());
        $traverser = new NodeTraverser();
        $visitor = new NodeVisitor([
            Text::class,
        ]);

        foreach ($files as $file) {
            $content = file_get_contents($file);

            if (false === $content) {
                continue;
            }

            try {
                $stmts = $parser->parse($content);
            } catch (Error) {
                continue;
            }

            if (null === $stmts) {
                continue;
            }

            $traverser->addVisitor(new NameResolver());
            $traverser->addVisitor($visitor);
            $traverser->traverse($stmts);
        }

        $translationDir = Path::getLanguageFolder();
        $translationFiles = glob($translationDir . DIRECTORY_SEPARATOR . '*.php', GLOB_BRACE);

        if (false === $translationFiles) {
            $output->writeln('<error>Failed to find files.</error>');
            return Command::FAILURE;
        }

        /** @var array<string, array<string, string>> $translationsCleared */
        $translationsCleared = [];

        $translationsRemovedCounter = 0;

        foreach ($translationFiles as $translationFile) {
            $translation = include $translationFile;
            $translationAll = $translation;

            $pathInfo = new PathInfo($translationFile);
            $fileName = (string) $pathInfo->getFileName();
            $fileNameParts = explode('.', $fileName);
            $languageCode = array_pop($fileNameParts);

            $messageDomain = 'default';

            if ([] !== $fileNameParts) {
                $messageDomain = implode('.', $fileNameParts);
            }

            $domainSlug = $languageCode;

            if ('default' !== $messageDomain) {
                $domainSlug = $messageDomain . '.' . $domainSlug;
            }

            foreach ($visitor->getConstructorArguments() as $constructorArgument) {
                $translationTextValue = $constructorArgument['arguments']['text'];
                $translationMessageDomain = $constructorArgument['arguments']['messageDomain'] ?? 'default';

                if ($messageDomain !== $translationMessageDomain) {
                    continue;
                }

                if (isset($translation[$translationTextValue])) {
                    unset($translation[$translationTextValue]);
                }
            }

            $translationsCleared[$domainSlug] = array_diff($translationAll, $translation);
            $translationsRemovedCounter += is_countable($translation) ? count($translation) : 0;
        }

        if (!$this->translationWriter->saveTranslations($translationsCleared)) {
            $output->writeln('<error>Failed to remove unused translations.</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully removed unused translations.</info>');
        $output->writeln($translationsRemovedCounter . ' translation' . ($translationsRemovedCounter > 1 ? 's have' : ' has') . ' been found and removed.');
        return Command::SUCCESS;
    }
}
