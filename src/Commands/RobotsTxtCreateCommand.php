<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use Kiwa\Config\RobotsTxtContent;
use Kiwa\Config\RobotsTxtFile;
use Kiwa\Path;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RobotsTxtCreateCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class RobotsTxtCreateCommand extends Command
{
    /**
     * RobotsTxtCreateCommand constructor.
     *
     * @param ConfigWriter $configWriter
     */
    public function __construct(
        private readonly ConfigWriter $configWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('robotstxt:create')
            ->setDescription('Creates a robots.txt file.')
            ->setHelp('This command will create a robots.txt file.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $settings = [];
        $mainURL = $this->configWriter->hasValue('mainURL')
            ? $this->configWriter->getValue('mainURL')
            : ''
        ;
        $sitemapFile = Path::getCrawlerFolder() . DIRECTORY_SEPARATOR . 'sitemap.xml';
        
        if (file_exists($sitemapFile)) {
            $settings['sitemap'] = PHP_EOL . 'Sitemap: ' . $mainURL . '/sitemap.xml';
        }
        
        $content = new RobotsTxtContent($settings);
        $created = RobotsTxtFile::write($content);

        if (!$created) {
            $output->writeln('<info>Error while creating robots.txt file</info>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully created robots.txt file</info>');
        return Command::SUCCESS;
    }
}
