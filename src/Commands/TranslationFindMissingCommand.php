<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use BitAndBlack\PathInfo\PathInfo;
use BitAndBlack\SentenceConstruction;
use Composer\InstalledVersions;
use ErrorException;
use JsonException;
use Kiwa\DI;
use Kiwa\Language\Text;
use Kiwa\Path;
use Kiwa\TemplateConsole\FileFinder;
use Kiwa\TemplateConsole\NodeVisitor;
use Kiwa\TemplateConsole\Translation\GoogleTranslate;
use Kiwa\TemplateConsole\Writers\TranslationWriter;
use PhpParser\Error;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\ParserFactory;
use PhpParser\PhpVersion;
use Places2Be\Locales\Exception;
use Places2Be\Locales\LanguageCode;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Terminal;

/**
 * Class TranslationFindMissingCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 * @see \Kiwa\TemplateConsole\Tests\Commands\TranslationFindMissingCommandTest
 */
class TranslationFindMissingCommand extends Command
{
    /**
     * TranslationFindMissingCommand constructor.
     *
     * @param TranslationWriter $translationWriter
     */
    public function __construct(
        private readonly TranslationWriter $translationWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('translation:find-missing')
            ->setDescription('Finds missing translations.')
            ->setHelp(
                'This command finds missing translations that have not been added yet.' . PHP_EOL .
                'This means, Kiwa searches for texts that have been echoed with the ' .
                '<comment>Kiwa\Language\Text</comment> class and proofs if they exist in the language files.' . PHP_EOL .
                'If the <comment>stichoza/google-translate-php</comment> library has been installed, ' .
                'Kiwa can also call the Google Translate API for a translation.'
            )
            ->addArgument(
                'path',
                InputArgument::OPTIONAL,
                'The path from where the translations should be extracted. This is <comment>/html</comment> and <comment>/src</comment> per default.'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ErrorException
     * @throws JsonException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var string|null $path */
        $path = $input->getArgument('path');

        $files = [];

        if (null === $path) {
            array_push(
                $files,
                ...FileFinder::findPhtmlFiles(
                    Path::getRootFolder(),
                )
            );
        }

        if (null !== $path) {
            if (!is_dir($path)) {
                throw new ErrorException('Failed to read path "' . $path . '".');
            }

            array_push(
                $files,
                ...FileFinder::findPhtmlFiles(
                    $path,
                )
            );
        }

        $parserFactory = new ParserFactory();
        $parser = $parserFactory->createForVersion(PhpVersion::getHostVersion());
        $traverser = new NodeTraverser();
        $visitor = new NodeVisitor([
            Text::class,
        ]);

        foreach ($files as $file) {
            $content = file_get_contents($file);

            if (false === $content) {
                continue;
            }

            try {
                $stmts = $parser->parse($content);
            } catch (Error) {
                continue;
            }

            if (null === $stmts) {
                continue;
            }

            $traverser->addVisitor(new NameResolver());
            $traverser->addVisitor($visitor);
            $traverser->traverse($stmts);
        }

        $translationDir = Path::getLanguageFolder();
        $translationFiles = glob($translationDir . DIRECTORY_SEPARATOR . '*.php', GLOB_BRACE);

        if (false === $translationFiles) {
            $output->writeln('<error>Failed to find files.</error>');
            return Command::FAILURE;
        }

        /** @var array<string, array<string, string>> $translationsMissing */
        $translationsMissing = [];

        foreach ($translationFiles as $translationFile) {
            $translation = include $translationFile;

            $pathInfo = new PathInfo($translationFile);
            $fileName = (string) $pathInfo->getFileName();
            $fileNameParts = explode('.', $fileName);

            $languageCode = array_pop($fileNameParts);

            $fileMessageDomain = 'default';

            if ([] !== $fileNameParts) {
                $fileMessageDomain = implode('.', $fileNameParts);
            }

            foreach ($visitor->getConstructorArguments() as $constructorArgument) {
                $translationTextValue = $constructorArgument['arguments']['text'];
                $translationMessageDomain = $constructorArgument['arguments']['messageDomain'] ?? 'default';

                if ($fileMessageDomain !== $translationMessageDomain) {
                    continue;
                }

                $domainSlug = $languageCode;

                if ('default' !== $fileMessageDomain) {
                    $domainSlug = $translationMessageDomain . '.' . $domainSlug;
                }

                if (!isset($translation[$translationTextValue])) {
                    if (isset($translationsMissing[$translationTextValue])
                        && in_array($domainSlug, $translationsMissing[$translationTextValue], true)
                    ) {
                        continue;
                    }

                    $translationsMissing[$translationTextValue][] = $domainSlug;
                }
            }
        }

        if ([] === $translationsMissing) {
            $output->writeln('<info>Couldn\'t find any missing translations.</info>');
            return Command::SUCCESS;
        }

        $output->writeln('The following translations are missing:');

        $column2WidthMinimum = 10;
        $tableBorderWidth = 7;

        foreach ($translationsMissing as $values) {
            $languageCodesList = (string) new SentenceConstruction(', ', ' and ', array_values($values));
            $languageCodesListLength = strlen($languageCodesList);

            if ($languageCodesListLength > $column2WidthMinimum) {
                $column2WidthMinimum = $languageCodesListLength;
            }
        }

        $terminal = new Terminal();
        $terminalWidth = $terminal->getWidth();

        $column2Width = $column2WidthMinimum;
        $column1Width = $terminalWidth - $column2Width - $tableBorderWidth;

        $table = new Table($output);
        $table
            ->setColumnMaxWidth(0, $column1Width)
            ->setColumnMaxWidth(1, $column2Width)
            ->setColumnWidths([
                $column1Width,
                $column2Width,
            ])
            ->setStyle('box-double')
            ->setHeaders(['Word or sentence', 'Missing in message domain'])
        ;

        $arrayKeys = array_keys($translationsMissing);
        $lastArrayKey = array_pop($arrayKeys);

        foreach ($translationsMissing as $key => $values) {
            $languageCodesList = new SentenceConstruction(
                '</info>, <info>',
                '</info> and <info>',
                array_values($values)
            );

            $addition = '...';

            $keyStriped = mb_substr(
                (string) $key,
                0,
                $column1Width - mb_strlen($addition)
            );

            if ($keyStriped !== $key) {
                $keyStriped .= $addition;
            }

            $table->addRow([
                $keyStriped,
                '<info>' . $languageCodesList . '</info>',
            ]);

            if ($lastArrayKey !== $key) {
                $table->addRow(
                    new TableSeparator()
                );
            }
        }

        $table->render();

        /** @var QuestionHelper $helper */
        $helper = $this->getHelper('question');

        $question = new Question('Kiwa may insert those parts into your language files so you have an easier start when translating them. Do you want to update your files? Enter <info>yes</info> or press <comment>enter</comment> to abort. ', false);
        $tryToTranslate = $helper->ask($input, $output, $question);

        if (false === $tryToTranslate) {
            $output->writeln('Done.');
            return Command::SUCCESS;
        }

        /** @var string|null $defaultLanguageCode */
        $defaultLanguageCode = DI::getConfig()->hasValue('languageCode')
            ? DI::getConfig()->getValue('languageCode')
            : null
        ;

        $isLibraryInstalled = InstalledVersions::isInstalled('stichoza/google-translate-php', true);
        $translationPossible = $isLibraryInstalled && null !== $defaultLanguageCode;

        if ($translationPossible) {
            $question = new Question('Kiwa can try to automatically translate your contents. This may help you to faster get your content finished. Do you want to give it a try? Enter <info>yes</info> or press <comment>enter</comment> to abort. ', false);
            $tryToTranslate = $helper->ask($input, $output, $question);
        } else {
            $output->writeln('<comment>Note:</comment> Kiwa can try to automatically translate your contents. To use this function, you need to: ');

            if (null === $defaultLanguageCode) {
                $output->writeln('- Add a default language code to your `config.php`, for example `setDefaultLanguageCode(\'en\')`.');
            }

            $output->writeln('- Add `stichoza/google-translate-php` to your (dev) dependencies.');
            $tryToTranslate = false;
        }

        $translationsNew = [];
        $translationsMissingCount = count($translationsMissing, COUNT_RECURSIVE);
        $ignoreTranslationErrors = false;
        $progressBar = new ProgressBar($output, $translationsMissingCount);

        foreach ($translationsMissing as $translationMissing => $missingLanguages) {
            foreach ($missingLanguages as $missingLanguage) {
                /**
                 * If the language is not loaded yet, we load it here.
                 */
                if (!array_key_exists($missingLanguage, $translationsNew)) {
                    $translationFile = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . $missingLanguage . '.php';
                    $translationContent = [];

                    if (file_exists($translationFile)) {
                        $translationContent = include $translationFile;
                    }

                    $translationsNew[$missingLanguage] = $translationContent;
                }

                $progressBar->advance();

                /**
                 * The default language doesn't need to be translated, so we skip here.
                 */
                if ($defaultLanguageCode === $missingLanguage) {
                    continue;
                }

                /**
                 * Country-specific languages can't get translated, so we take the short version
                 * and skip if it is equal to the source language code.
                 */
                try {
                    $defaultLanguageCodeShort = (new LanguageCode((string) $defaultLanguageCode))->getLanguageCodeShort();
                    $missingLanguageShort = (new LanguageCode($missingLanguage))->getLanguageCodeShort();

                    if ($defaultLanguageCodeShort === $missingLanguageShort) {
                        continue;
                    }
                } catch (Exception) {
                    $defaultLanguageCodeShort = $defaultLanguageCode;
                    $missingLanguageShort = null;
                }

                $translationTranslated = null;

                if (false !== $tryToTranslate && null !== $defaultLanguageCode) {
                    $googleTranslate = new GoogleTranslate();
                    $translationTranslated = $googleTranslate->getTranslation(
                        (string) $defaultLanguageCodeShort,
                        $missingLanguageShort ?? $missingLanguage,
                        (string) $translationMissing
                    );

                    if (null === $translationTranslated && false === $ignoreTranslationErrors) {
                        $output->writeln('');

                        /** @var QuestionHelper $helper */
                        $helper = $this->getHelper('question');

                        $question = new ChoiceQuestion(
                            'There was an error while trying to translate your content. How do you want to continue? (Defaults to <info>Abort</info>.)',
                            [
                                'abort' => 'Abort the whole process (default).',
                                'continue' => 'Continue adding the untranslated parts in the language files.',
                            ],
                            'abort'
                        );
                        $question->setErrorMessage('Selection "%s" is invalid.');

                        $continue = $helper->ask($input, $output, $question);

                        if ('abort' === $continue) {
                            break 2;
                        }

                        $ignoreTranslationErrors = true;
                    }
                }

                $translationsNew[$missingLanguage][$translationMissing] = $translationTranslated ?? $translationMissing;
            }
        }

        $progressBar->finish();
        $output->writeln('');

        if (!$this->translationWriter->saveTranslations($translationsNew)) {
            $output->writeln('<error>Failed adding missing translations</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully added missing translations</info>');
        return Command::SUCCESS;
    }
}
