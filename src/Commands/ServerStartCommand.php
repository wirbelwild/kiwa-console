<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use Kiwa\Path;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\PhpExecutableFinder;
use Symfony\Component\Process\Process;

/**
 * The ServerStartCommand allows running a PHP server on a local machine.
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class ServerStartCommand extends Command
{
    /**
     * @var Process<int, mixed>|null
     */
    private ?Process $process = null;

    private readonly string $root;
    
    private readonly string $hostPort;

    /**
     * ServerStartCommand constructor.
     */
    public function __construct()
    {
        $this->hostPort = '127.0.0.1:8000';
        $this->root = Path::getPublicFolder() . DIRECTORY_SEPARATOR . 'index.php';
        parent::__construct();
    }

    /**
     * Configuration.
     */
    protected function configure(): void
    {
        $this
            ->setName('server:start')
            ->setDescription('Starts a local PHP server.')
            ->setHelp(
                'This command starts a local PHP server. It allows to run your project on a local machine.' . PHP_EOL .
                'The server runs in both <info>prod</info> and <info>dev</info> mode, but should not used on a live website.'
            )
            ->addArgument(
                'host and port',
                InputArgument::OPTIONAL,
                'Define your server\'s address here. This is <info>' . $this->hostPort . '</info> per default.'
            )
            ->addArgument(
                'root',
                InputArgument::OPTIONAL,
                'Define your root folder here. This is <info>' . $this->root . '</info> per default.' . PHP_EOL .
                    'A relative path is also okay.'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $hostPortNew = $input->getArgument('host and port');
        $hostPortPreviously = false;
        $hostPortCurrent = '';

        $root = $input->getArgument('root') ?? $this->root;

        $connectionFile = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.local-server-preferred';
        
        if (file_exists($connectionFile)) {
            $hostPortPreviously = file_get_contents($connectionFile);
        }

        $hasSetNewConnection = true === is_string($hostPortNew);
        $hasPreviousConnection = false !== $hostPortPreviously;
        
        if (true === $hasSetNewConnection) {
            $hostPortCurrent = $hostPortNew;
        }

        if (!$hasSetNewConnection && $hasPreviousConnection) {
            $hostPortCurrent = (string) $hostPortPreviously;
        }

        if (!$hasSetNewConnection && !$hasPreviousConnection) {
            $hostPortCurrent = $this->hostPort;
        }
        
        $hostPortCurrent = explode(':', (string) $hostPortCurrent);
        $host = $hostPortCurrent[0];
        $port = (int) $hostPortCurrent[1];

        if (false === $this->isPortFree($host, $port)) {
            if ($hasSetNewConnection) {
                $output->write('Preferred port ' . $port . ' is already in use. ');
                $output->writeln('<error>Can\'t start server.</error>');
                return Command::FAILURE;
            }
            
            if (null === $port = $this->getNearestFreePort($host, $port)) {
                $port = $this->getFreePort();
            }
            
            if (null === $port) {
                $output->writeln('<error>Can\'t start server.</error>');
                return Command::FAILURE;
            }

            $output->writeln('Using free port ' . $port . '.');
        }
               
        if ($hasSetNewConnection || !file_exists($connectionFile)) {
            file_put_contents(
                $connectionFile,
                $host . ':' . $port
            );
        }

        $message = 'Starting local server at http://' . $host . ':' . $port . '.';
        
        $output->writeln('<fg=black;bg=green>  ' . str_pad('', strlen($message)) . '  </>');
        $output->writeln('<fg=black;bg=green>  ' . $message . '  </>');
        $output->writeln('<fg=black;bg=green>  ' . str_pad('', strlen($message)) . '  </>');
        $output->writeln('Press CTRL+C to stop the server.');
        
        if (function_exists('pcntl_signal')) {
            $this->startSignal();
        }
        
        $this->process = new Process([$this->getPHPBinary(), '-S', $host . ':' . $port, $root]);
        $this->process->setTimeout(null);
        $this->process->run(static function ($type, $data) use ($output) {
            $output->write($data);
        });

        if (function_exists('pcntl_signal')) {
            $this->endSignal();
        }
        
        $output->writeln('');
        $output->writeln('<info>The server has been stopped.</info>');
        
        return Command::SUCCESS;
    }

    /**
     * Finds and returns a free port.
     *
     * @return int|null
     */
    private function getFreePort(): ?int
    {
        if (!function_exists('socket_create_listen')) {
            return null;
        }
        
        $sock = socket_create_listen(0);
        
        if (!is_resource($sock)) {
            return null;
        }
        
        socket_getsockname($sock, $address, $port);
        socket_close($sock);
        unset($address);
        return $port;
    }

    /**
     * Checks if a port is free and may be used.
     *
     * @param string $host
     * @param int $port
     * @return bool
     */
    private function isPortFree(string $host, int $port): bool
    {
        $nullErrorHandler = set_error_handler(static fn () => true);

        $connection = fsockopen($host, $port, $errorNumber, $errorMessage, 1);
        $isResource = is_resource($connection);
        
        if (false !== $connection) {
            fclose($connection);
        }

        set_error_handler($nullErrorHandler);
        return !$isResource;
    }

    /**
     * Checks if a process is still running and ends it if so.
     */
    private function closeConnection(): void
    {
        if ($this->process?->isRunning()) {
            $this->process->stop();
        }
    }

    /**
     * Finds and returns the PHP binary.
     *
     * @return string
     */
    private function getPHPBinary(): string
    {
        $phpExecutable = new PhpExecutableFinder();
        $phpBinaryPath = $phpExecutable->find();
        return false !== $phpBinaryPath ? $phpBinaryPath : 'php';
    }
    
    /**
     * Searches for the nearest free port. This method is used to provide nice handling of
     * multiple servers running.
     *
     * @param string $host
     * @param int $port
     * @return int|null
     */
    private function getNearestFreePort(string $host, int $port): ?int
    {
        $portMin = 1024;
        $portMax = 65535;
        $startedFromNew = false;
        $counter = $port + 1;
        
        while (true) {
            if ($this->isPortFree($host, $counter)) {
                return $counter;
            }
            
            if ($counter >= $portMax) {
                if ($startedFromNew) {
                    break;
                }

                $counter = $portMin;
                $startedFromNew = true;
            }

            ++$counter;
        }
        
        return null;
    }

    /**
     * Starts a signal.
     */
    private function startSignal(): void
    {
        declare(ticks=1);

        pcntl_signal(
            SIGHUP,
            function () {
                $this->closeConnection();
            }
        );

        pcntl_signal(
            SIGINT,
            function () {
                $this->closeConnection();
            }
        );

        pcntl_signal(
            SIGTERM,
            function () {
                $this->closeConnection();
            }
        );
    }

    /**
     * Ends a signal.
     */
    private function endSignal(): void
    {
        pcntl_signal(SIGHUP, SIG_DFL);
        pcntl_signal(SIGINT, SIG_DFL);
        pcntl_signal(SIGTERM, SIG_DFL);
    }
}
