<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use Kiwa\Path;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CacheClearCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class CacheClearCommand extends Command
{
    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('cache:clear')
            ->setDescription('Deletes all cache files.')
            ->setHelp('Deletes all cache files.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $files = [];
        $cacheFolder = Path::getCacheFolder() . DIRECTORY_SEPARATOR;

        $htmlFiles = glob($cacheFolder . '*.html');
        
        if (is_array($htmlFiles) && [] !== $htmlFiles) {
            array_push($files, ...$htmlFiles);
        }

        $containerFile = $cacheFolder . 'CompiledContainer.php';

        if (file_exists($containerFile)) {
            $files[] = $containerFile;
        }

        $routesFile = $cacheFolder . 'routes.php';

        if (file_exists($routesFile)) {
            $files[] = $routesFile;
        }
        
        if (empty($files)) {
            $output->writeln('<info>There were no cache files found.</info>');
            return Command::SUCCESS;
        }

        foreach ($files as $key => $file) {
            if (unlink($file)) {
                unset($files[$key]);
            }
        }
        
        if (!empty($files)) {
            $output->writeln('<error>Error while deleting cache files.</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully deleted all cache files.</info>');
        return Command::SUCCESS;
    }
}
