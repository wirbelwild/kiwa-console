<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use axy\htpasswd\PasswordFile;
use Kiwa\Config;
use Kiwa\Config\HtaccessContent;
use Kiwa\Config\HtaccessFile;
use Kiwa\Connect\Controller;
use Kiwa\DI;
use Kiwa\Path;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class HtaccessCreateCommand extends Command
{
    private readonly Config $config;

    /**
     * ConfigUpdateCommand constructor.
     */
    public function __construct()
    {
        $controller = new Controller();
        $this->config = DI::getConfig();
        parent::__construct();
        unset($controller);
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('htaccess:create')
            ->setDescription('Creates a .htaccess file.')
            ->setHelp('This command creates a .htaccess file depending on your config. If the config holds values for a .htpasswd file, it will create that too.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        /**
         * @var array{
         *     useSSL: bool,
         *     useWWW: bool,
         * } $configHtaccess
         */
        $configHtaccess = $this->config->hasValue('htaccess')
            ? $this->config->getValue('htaccess')
            : [
                'useSSL' => false,
                'useWWW' => false,
            ]
        ;

        $configHtaccess['useHtpasswd'] = false;
        $useHtpasswd = $this->config->hasValue('useHtpasswd') && (bool) $this->config->getValue('useHtpasswd');
        
        $htpasswdPath = Path::getRootFolder() . DIRECTORY_SEPARATOR . '.htpasswd';

        if (file_exists($htpasswdPath)) {
            unlink($htpasswdPath);
        }
        
        if ($useHtpasswd) {
            $htpasswdUser = $this->config->hasValue('htpasswdUser')
                ? $this->config->getValue('htpasswdUser')
                : null
            ;

            if (false === is_string($htpasswdUser)) {
                $htpasswdUser = null;
            }

            $htpasswdPassword = $this->config->hasValue('htpasswdPassword')
                ? $this->config->getValue('htpasswdPassword')
                : null
            ;

            if (false === is_string($htpasswdPassword)) {
                $htpasswdPassword = null;
            }

            $htpasswdPathPrefix = $this->config->hasValue('htpasswdPathPrefix')
                ? $this->config->getValue('htpasswdPathPrefix')
                : null
            ;

            if (true === is_string($htpasswdPathPrefix)) {
                $htpasswdPathPrefix = rtrim($htpasswdPathPrefix, DIRECTORY_SEPARATOR);
            }

            if (false === is_string($htpasswdPathPrefix)) {
                $htpasswdPathPrefix = null;
            }

            if (null !== $htpasswdUser && null !== $htpasswdPassword) {
                $passwordFile = new PasswordFile($htpasswdPath);
                $passwordFile->setPassword($htpasswdUser, $htpasswdPassword);
                $passwordFile->save();
                $configHtaccess['useHtpasswd'] = $htpasswdPathPrefix . $htpasswdPath;
            }
        }
        
        $htaccess = new HtaccessContent($configHtaccess);

        if (method_exists(HtaccessFile::class, 'add')) {
            $created = HtaccessFile::add($htaccess, 'kiwa/core');
        }
        /**
         * @todo Remove in v2.0.
         */
        else {
            $created = HtaccessFile::write($htaccess);
        }

        if (!$created) {
            $output->writeln('<info>Error while creating .htacces file</info>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully created .htacces file</info>');
        return Command::SUCCESS;
    }
}
