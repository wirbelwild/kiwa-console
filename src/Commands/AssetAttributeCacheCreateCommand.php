<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use BitAndBlack\Composer\Composer;
use BitAndBlack\ImageInformation\Exception\ExtensionNotSupportedException;
use BitAndBlack\ImageInformation\Exception\FileNotFoundException;
use BitAndBlack\ImageInformation\Image;
use BitAndBlack\ImageInformation\Source\File;
use Codewithkyrian\Transformers\Exceptions\UnsupportedTaskException;
use Codewithkyrian\Transformers\Transformers;
use Codewithkyrian\Transformers\Utils\ImageDriver;
use Composer\InstalledVersions;
use DateTimeImmutable;
use GdImage;
use Imagick;
use Kiwa\DI;
use Kiwa\Language\Text;
use Kiwa\Path;
use Kiwa\SourceCollection\AttributeCacheFileName;
use Kiwa\TemplateConsole\Translation\GoogleTranslate;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use function Codewithkyrian\Transformers\Pipelines\pipeline;

class AssetAttributeCacheCreateCommand extends Command
{
    protected function configure(): void
    {
        $this
            ->setName('asset:attribute-cache:create')
            ->setDescription('Creates a asset attribute cache.')
            ->setHelp('This command takes all asset files of a given folder and creates cache files containing some of their information. Those cache files can be used for the `kiwa/source-collection` library later.')
            ->addOption(
                'source-path',
                null,
                InputOption::VALUE_REQUIRED,
                'The source path where assets should be searched.'
            )
            ->addOption(
                'cache-path',
                null,
                InputOption::VALUE_REQUIRED,
                'The cache path where attribute cache files should be stored.'
            )
            ->addOption(
                'only-missing',
                'm',
                InputOption::VALUE_NONE,
                'Handle only assets that have no cache file yet.'
            )
            ->addOption(
                'transformers-cache-path',
                null,
                InputOption::VALUE_NONE,
                'The path where the transformer\'s data should be stored. This takes only effect if you use an image captioning model to generate image descriptions.'
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws UnsupportedTaskException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if (false === Composer::classExists(AttributeCacheFileName::class)) {
            $io->error('Failed to create attribute cache files.');
            $io->note('The `kiwa/source-collection` library seems to miss. Please install it at first.');
            return Command::FAILURE;
        }

        $sourcePath = $input->getOption('source-path');

        if (false === is_string($sourcePath)) {
            $sourcePath = Path::getBuildFolder();
        }

        $cachePath = $input->getOption('cache-path');

        if (false === is_string($cachePath)) {
            $cachePath = Path::getRootFolder() . DIRECTORY_SEPARATOR . 'attributes-cache';
        }

        if (false === is_dir($cachePath) && false === mkdir($cachePath) && false === is_dir($cachePath)) {
            $io->error('Failed to create attribute cache folder "' . $cachePath . '".');
            return Command::FAILURE;
        }

        /** @var bool $onlyMissing */
        $onlyMissing = $input->getOption('only-missing');

        $transformersCachePath = $input->getOption('transformers-cache-path');

        if (false === is_string($transformersCachePath)) {
            $transformersCachePath = null;
        }

        $hasImagickInstalled = Composer::classExists(Imagick::class);
        $hasGdInstalled = Composer::classExists(GdImage::class);
        $hasImageLibraryInstalled = $hasImagickInstalled || $hasGdInstalled;
        $canHandleImageSizes = Composer::classExists(Image::class) && $hasImageLibraryInstalled;

        if (false === $canHandleImageSizes) {
            $io->note('Cannot extract image\'s size. If you want to add the image size automatically, make sure to add the `bitandblack/image-information` library.');
        }

        $canGenerateImageDescriptions = Composer::classExists(Transformers::class);

        if (false === $canGenerateImageDescriptions) {
            $io->note('Image descriptions can be generated using the image captioning model `nlpconnect/vit-gpt2-image-captioning`. To use it, add the `codewithkyrian/transformers` library to your project and follow their installation guidelines.');
        }

        $formats = [
            'jpg',
            'jpeg',
            'gif',
            'webp',
            'avif',
            'tif',
            'tiff',
            'png',
            'mp3',
            'mp4',
            'webm',
            'mov',
            'wav',
        ];

        $pattern = '/\.(' . implode('|', $formats) . ')$/';

        $finder = new Finder();
        $finder
            ->files()
            ->in($sourcePath)
            ->name($pattern)
        ;

        $filesHandledCounter = 0;
        $filesExistedBefore = 0;

        $hasKiwaTranslationPossibility = Composer::classExists(Text::class);
        $hasKiwaTranslation = false;

        /** @var string|null $defaultLanguageCode */
        $defaultLanguageCode = DI::getConfig()->getValue('languageCode');

        $isTranslationNeeded = null !== $defaultLanguageCode
            && 'en' !== $defaultLanguageCode
            && true !== str_starts_with($defaultLanguageCode, 'en-')
        ;

        $tryToTranslate = false;
        $googleTranslate = new GoogleTranslate();

        if (true === $isTranslationNeeded) {
            $isLibraryInstalled = InstalledVersions::isInstalled('stichoza/google-translate-php', true);

            /** @var QuestionHelper $helper */
            $helper = $this->getHelper('question');

            if (true === $isLibraryInstalled) {
                $question = new Question('Kiwa can try to automatically translate the asset descriptions into your default language <comment>' . $defaultLanguageCode . '</comment>. Do you want to give it a try? Enter <info>yes</info> or press <comment>enter</comment> to abort. ', false);
                $tryToTranslate = $helper->ask($input, $output, $question);
            } else {
                $output->writeln('<comment>Note:</comment> Kiwa can try to automatically translate the asset descriptions into your default language <comment>' . $defaultLanguageCode . '</comment>. To use this function, you need to: ');
                $output->writeln('- Add `stichoza/google-translate-php` to your (dev) dependencies.');
            }

            $tryToTranslate = false !== $tryToTranslate;
        }

        $progressBar = new ProgressBar($output, count($finder));
        $progressBar->start();

        foreach ($finder as $file) {
            $progressBar->advance();

            $fileName = (string) $file;

            if (Path::getBuildFolder() === $sourcePath) {
                /** @var string $fileName */
                $fileName = str_replace(Path::getPublicFolder(), '', $file);
            }

            $fileName = (string) new AttributeCacheFileName($fileName);
            $cacheFile = $cachePath . DIRECTORY_SEPARATOR . $fileName;

            if (true === $onlyMissing && true === file_exists($cacheFile)) {
                ++$filesExistedBefore;
                continue;
            }

            $attributes = [];

            if (true === $canHandleImageSizes) {
                try {
                    $imageFile = new File($file);
                    $image = new Image($imageFile);
                    $size = $image->getSize();
                    $attributes['width'] = (int) round($size['width']);
                    $attributes['height'] = (int) round($size['height']);
                } catch (FileNotFoundException|ExtensionNotSupportedException) {
                }
            }

            if (true === $canGenerateImageDescriptions) {
                $driver = match (true) {
                    $hasImagickInstalled => ImageDriver::IMAGICK,
                    $hasGdInstalled => ImageDriver::GD,
                    default => null,
                };

                $imageDescriptionResult = null;

                if (null !== $driver) {
                    $transformers = Transformers::setup()
                        ->setImageDriver($driver)
                    ;

                    if (null !== $transformersCachePath) {
                        $transformers->setCacheDir($transformersCachePath);
                    }

                    $pipeline = pipeline('image-to-text');
                    $imageDescriptionResult = $pipeline($file);
                }

                $imageDescription = null;

                if (true === is_array($imageDescriptionResult)) {
                    $imageDescription = $imageDescriptionResult[0]['generated_text'] ?? null;
                }

                if (null !== $imageDescription) {
                    $imageDescription = mb_ucfirst($imageDescription);

                    if (true === $hasKiwaTranslationPossibility) {
                        if (true === $tryToTranslate && null !== $defaultLanguageCode) {
                            $imageDescription = $googleTranslate->getTranslation(
                                'en',
                                $defaultLanguageCode,
                                $imageDescription,
                            );
                        }

                        $imageDescription = 'new Text(\'' . $imageDescription . '\')';
                        $hasKiwaTranslation = true;
                    }

                    $attributes['alt'] = $imageDescription;
                    $attributes['title'] = $imageDescription;
                }
            }

            $array = array_map(
                static function (string $attributeKey, string|bool|int|float $attributeValue): string {
                    $attributeValueExported = var_export($attributeValue, true);

                    if (str_starts_with((string) $attributeValue, 'new Text')) {
                        $attributeValueExported = $attributeValue;
                    }

                    return '    \'' . $attributeKey . '\' => ' . $attributeValueExported . ',';
                },
                array_keys($attributes),
                array_values($attributes)
            );
            $array = '['
                . PHP_EOL
                . implode(PHP_EOL, $array)
                . PHP_EOL
                . ']'
            ;

            $now = new DateTimeImmutable('now');
            $import = '';

            if (true === $hasKiwaTranslation) {
                $import = PHP_EOL . 'use Kiwa\\Language\\Text;';
            }

            $fileContent = <<<CODE
            <?php

            /**
             * This file has been created at {$now->format('Y-m-d H:i:s')}
             * using the `asset:attribute-cache:create` command
             * from the `kiwa/console` library.
             */
            $import

            return $array;
            
            CODE;

            file_put_contents(
                $cacheFile,
                $fileContent
            );

            ++$filesHandledCounter;
        }

        $progressBar->finish();

        if (0 === $filesHandledCounter) {
            $io->writeln('Found no files to handle. (' . $filesExistedBefore . ' have been skipped as they existed before.)');
            return Command::SUCCESS;
        }

        $io->success('Created ' . $filesHandledCounter . ' file' . (1 === $filesHandledCounter ? '' : 's') . '.');
        return Command::SUCCESS;
    }
}
