<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use Kiwa\Path;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class InstallAssetsCommand
 *
 * @package Kiwa\Command
 */
class InstallAssetsCommand extends Command
{
    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('assets:install')
            ->setDescription('Installs all asset files.')
            ->setHelp('This command installs asset files coming from Node.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        exec('npm -v', $npmVersion, $npmExitCode);
        exec('yarn -v', $yarnVersion, $yarnExitCode);
        
        if (0 !== $yarnExitCode && 0 !== $npmExitCode) {
            $output->writeln('<error>Can\'t find NPM or Yarn.</error> Install one of them at first.');
            return Command::FAILURE;
        }
        
        if (!file_exists(Path::getRootFolder() . DIRECTORY_SEPARATOR . 'package.json')) {
            $output->writeln('<error>No package.json found.</error>');
            return Command::FAILURE;
        }

        if (0 === $yarnExitCode) {
            $output->writeln('Yarn is installed in version ' . $yarnVersion[0] . ' and will be used for installing dependencies now.');
            passthru('yarn install');
        } elseif (0 === $npmExitCode) {
            $output->writeln('NPM is installed in version ' . $npmVersion[0] . ' and will be used for installing dependencies now.');
            passthru('npm install');
        }

        return Command::SUCCESS;
    }
}
