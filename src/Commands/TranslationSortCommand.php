<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use BitAndBlack\PathInfo\PathInfo;
use Kiwa\Path;
use Kiwa\TemplateConsole\Writers\TranslationWriter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TranslationSortCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class TranslationSortCommand extends Command
{
    /**
     * TranslationSortCommand constructor.
     *
     * @param TranslationWriter $translationWriter
     */
    public function __construct(
        private readonly TranslationWriter $translationWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('translation:sort')
            ->setDescription('Sorts all translations.')
            ->setHelp('This command sorts all translation files by their key case insensitive.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $translationDir = Path::getLanguageFolder();
        
        $translationFiles = glob($translationDir . DIRECTORY_SEPARATOR . '*.php', GLOB_BRACE);

        if (false === $translationFiles) {
            $output->writeln('<error>Failed to find files.</error>');
            return Command::FAILURE;
        }

        $translationsNew = [];
        
        foreach ($translationFiles as $translationFile) {
            $translation = include $translationFile;
            $pathInfo = new PathInfo($translationFile);
            $fileName = $pathInfo->getFileName();
            
            uksort(
                $translation,
                static function (string $keyA, string $keyB): int {
                    $keyA = mb_strtolower($keyA);
                    $keyB = mb_strtolower($keyB);
                    return strcmp($keyA, $keyB);
                }
            );

            $translationsNew[$fileName] = $translation;
        }
        
        if (!$this->translationWriter->saveTranslations($translationsNew)) {
            $output->writeln('<error>Failed sorting translations</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully sorted translations</info>');
        return Command::SUCCESS;
    }
}
