<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use Kiwa\TemplateConsole\Environment;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConfigValidateCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class ConfigValidateCommand extends Command
{
    private string $consolePath;

    /**
     * @param ConfigWriter $configWriter
     */
    public function __construct(
        private readonly ConfigWriter $configWriter,
    ) {
        $this->consolePath = 'vendor/bin/kiwa-template';

        if (Environment::hasBeenCalledFromTemplate()) {
            $this->consolePath = 'bin/console';
        }
        
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('config:validate')
            ->setDescription('Validates the config file.')
            ->setHelp('This command validates the config file.')
        ;
    }

    /**
     * Validates the config file
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $config = $this->configWriter->getConfig();
        $valid = true;

        if (!isset($config['languageHandling'])) {
            $output->writeln('We recommend defining the <info>languageHandling</info>. To fix this run <info>php ' . $this->consolePath . ' language:define</info>');
            $valid = false;
        }

        if (!isset($config['languageCode'])) {
            $output->writeln('We recommend adding a default <info>languageCode</info>. To fix this run <info>php ' . $this->consolePath . ' language:define</info>');
            $valid = false;
        }

        if (!isset($config['htaccess'])) {
            $output->writeln('We recommend adding information about the <info>url structure</info>. To fix this run <info>php ' . $this->consolePath . ' url:define</info>');
            $valid = false;
        }

        $baseMetaTags = $config['baseMetaTags'];

        if (!isset($baseMetaTags['og:title'], $baseMetaTags['description'], $baseMetaTags['og:description'])
            || count($baseMetaTags['og:title']) !== count($baseMetaTags['description'])
            || count($baseMetaTags['description']) !== count($baseMetaTags['og:description'])
        ) {
            $output->writeln('<error>baseMetaTags are not set in all existing languages.</error>');
            $valid = false;
        }

        $languagesInMetaTags = array_keys($baseMetaTags['og:title'] ?? []);

        $pages = $config['pages'];
        $languagesInPages = array_keys($pages);

        if ($languagesInMetaTags !== $languagesInPages) {
            $output->writeln('<error>The language versions of your meta tags are not identical with the language versions of your pages.</error> If you haven\'t set some, run <info>php ' . $this->consolePath . ' basic:metatags</info>');
            $valid = false;
        }

        if (!isset($config['mainURL'])) {
            $output->writeln('There is no <info>mainURL</info> defined. This one is needed to create beautiful URLs.');
            $valid = false;
        }

        if ($valid) {
            $output->writeln('<info>Your config is valid.</info>');
            return Command::FAILURE;
        }

        $output->writeln('<info>There are some issues you should fix.</info>');
        return Command::SUCCESS;
    }
}
