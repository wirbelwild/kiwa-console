<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use Kiwa\Path;
use RuntimeException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FilesUpdateCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 */
class FilesUpdateCommand extends Command
{
    /**
     * @var array<string, string>
     */
    private readonly array $files;

    /**
     * Configuration.
     */
    protected function configure(): void
    {
        $this
            ->setName('files:update')
            ->setDescription('Updates all static files.')
            ->setHelp('This command updates all static files which are for example <info>bin/console</info> or <info>public/index.php</info>.')
        ;
    }

    /**
     * FilesUpdateCommand constructor.
     *
     * @param string|null $name
     */
    public function __construct(string|null $name = null)
    {
        $this->files = [
            'console' => Path::getRootFolder() . DIRECTORY_SEPARATOR . 'bin' . DIRECTORY_SEPARATOR . 'console',
            'index' => Path::getPublicFolder() . DIRECTORY_SEPARATOR . 'index.php',
        ];

        parent::__construct($name);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        foreach ($this->files as $fileName => $fileDestination) {
            $fileSource = dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . $fileName . '.php';
            
            $fileContent = (string) file_get_contents($fileSource);
            $fileContent = substr($fileContent, 18);
            
            $folder = dirname($fileDestination);
            
            if (!file_exists($folder) && !mkdir($folder) && !is_dir($folder)) {
                throw new RuntimeException(
                    sprintf('Directory "%s" was not created', $folder)
                );
            }
            
            $fileExistedBefore = file_exists($fileDestination);
            
            $success = false !== file_put_contents(
                $fileDestination,
                $fileContent
            );
            
            if (false === $success) {
                $output->writeln(
                    sprintf('<error>Couldn\'t ' . ($fileExistedBefore ? 'update' : 'create') . ' file "%s".</error>', $fileDestination)
                );
                continue;
            }

            $output->writeln(
                sprintf('Successfully ' . ($fileExistedBefore ? 'updated' : 'created') . ' file <info>%s</info>.', $fileDestination)
            );
        }

        return Command::SUCCESS;
    }
}
