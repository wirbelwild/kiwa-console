<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Commands;

use BitAndBlack\PathInfo\PathInfo;
use Kiwa\Path;
use Kiwa\TemplateConsole\Writers\TranslationWriter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class TranslationClearCommand
 *
 * @package Kiwa\TemplateConsole\Commands
 * @see \Kiwa\TemplateConsole\Tests\Commands\TranslationClearCommandTest
 */
class TranslationClearCommand extends Command
{
    /**
     * TranslationClearCommand constructor.
     *
     * @param TranslationWriter $translationWriter
     */
    public function __construct(
        private readonly TranslationWriter $translationWriter,
    ) {
        parent::__construct();
    }

    /**
     * Configuration
     */
    protected function configure(): void
    {
        $this
            ->setName('translation:clear')
            ->setDescription('Clears all translation files.')
            ->setHelp('This command removes all translations, where the source is equal to the translation itself.')
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $translationDir = Path::getLanguageFolder();
        
        $translationFiles = glob($translationDir . DIRECTORY_SEPARATOR . '*.php', GLOB_BRACE);
        $translationsNew = [];
        $removedTranslationsCount = 0;

        if (false === $translationFiles) {
            $output->writeln('<error>Failed to find files.</error>');
            return Command::FAILURE;
        }

        foreach ($translationFiles as $translationFile) {
            $translation = include $translationFile;
            $pathInfo = new PathInfo($translationFile);
            $fileName = $pathInfo->getFileName();
            
            foreach ($translation as $sourceText => $translatedText) {
                if ($sourceText === $translatedText) {
                    unset($translation[$sourceText]);
                    ++$removedTranslationsCount;
                }
            }
            
            $translationsNew[$fileName] = $translation;
        }
        
        if (!$this->translationWriter->saveTranslations($translationsNew)) {
            $output->writeln('<error>Failed clearing translations.</error>');
            return Command::FAILURE;
        }

        $output->writeln('<info>Successfully cleared translations.</info> ' . $removedTranslationsCount . ' have been removed.');
        return Command::SUCCESS;
    }
}
