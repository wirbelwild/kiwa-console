<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Translation;

use ErrorException;

class GoogleTranslate implements TranslationInterface
{
    /**
     * @param string $languageCodeSource
     * @param string $languageCodeTarget
     * @param string $text
     * @return string|null
     */
    public function getTranslation(string $languageCodeSource, string $languageCodeTarget, string $text): ?string
    {
        $googleTranslate = new \Stichoza\GoogleTranslate\GoogleTranslate();

        try {
            return $googleTranslate
                ->setSource($languageCodeSource)
                ->setTarget($languageCodeTarget)
                ->translate($text)
            ;
        } catch (ErrorException) {
            return null;
        }
    }
}
