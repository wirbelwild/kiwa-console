<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Translation;

interface TranslationInterface
{
    /**
     * @param string $languageCodeSource The language of the source text. This should be a language code like `en` or `en-gb`.
     * @param string $languageCodeTarget The target language in which the text should be translated. This should also be a language code like `en` or `en-gb`.
     * @param string $text The text that should be translated.
     * @return string|null
     */
    public function getTranslation(string $languageCodeSource, string $languageCodeTarget, string $text): ?string;
}
