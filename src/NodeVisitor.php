<?php

namespace Kiwa\TemplateConsole;

use PhpParser\Node;
use PhpParser\Node\Expr;
use PhpParser\Node\Expr\Array_;
use PhpParser\Node\Expr\ConstFetch;
use PhpParser\Node\Expr\New_;
use PhpParser\Node\Expr\Variable;
use PhpParser\Node\Name;
use PhpParser\Node\Scalar\LNumber;
use PhpParser\Node\Scalar\String_;
use PhpParser\NodeVisitorAbstract;
use ReflectionException;
use ReflectionMethod;

/**
 * Class NodeVisitor.
 *
 * @internal
 * @see \Kiwa\TemplateConsole\Tests\NodeVisitorTest
 */
class NodeVisitor extends NodeVisitorAbstract
{
    /**
     * @var array<int, array{
     *     class: class-string,
     *     arguments: array<string, string|int|float|bool|null>,
     * }>
     */
    private array $constructorArguments = [];

    /**
     * @param array<int, class-string> $classesToParse
     */
    public function __construct(
        private readonly array $classesToParse,
    ) {
    }

    /**
     * @throws ReflectionException
     */
    public function enterNode(Node $node): int|null|Node
    {
        //if ($node instanceof Node\Stmt\Expression && $node->expr instanceof Node\Expr\Assign) {
        //    $assign = $node->expr;
        //
        //    if ($assign->var instanceof Node\Expr\Variable && $assign->expr instanceof Node\Scalar) {
        //        $this->variableValues[$assign->var->name] = $this->getArgumentValue($assign->expr);
        //    }
        //}

        if ($node instanceof New_) {
            $className = $node->class instanceof Name ? $node->class->toString() : null;

            if (false === in_array($className, $this->classesToParse, true)) {
                return null;
            }

            $reflectionMethod = new ReflectionMethod($className, '__construct');
            $parameters = array_values(
                $reflectionMethod->getParameters()
            );

            $arguments = [];

            foreach ($node->getArgs() as $key => $arg) {
                $parameterName = $parameters[$key]->getName();

                $value = $this->getArgumentValue($arg->value);
                $containsVariable = null === $value;

                if (true === $containsVariable) {
                    return null;
                }

                $arguments[$parameterName] = $value;
            }

            $this->constructorArguments[] = [
                'class' => $className,
                'arguments' => $arguments,
            ];
        }

        return null;
    }

    private function getArgumentValue(Expr $value): string|int|null
    {
        if ($value instanceof String_) {
            return $value->value;
        }

        if ($value instanceof Array_) {
            return 'array';
        }

        if ($value instanceof LNumber) {
            return $value->value;
        }

        if ($value instanceof ConstFetch) {
            return $value->name->toString();
        }

        if ($value instanceof Variable) {
            return null;
            //return $this->variableValues[$value->name] ?? 'unknown';
        }

        return null;
    }

    /**
     * @return array<int, array{
     *     class: class-string,
     *     arguments: array<string, string|int|float|bool|null>
     * }>
     */
    public function getConstructorArguments(): array
    {
        return array_unique($this->constructorArguments, SORT_REGULAR);
    }
}
