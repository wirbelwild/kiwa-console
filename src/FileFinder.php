<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole;

use Generator;
use Kiwa\Path;
use Symfony\Component\Finder\Finder;

/**
 * @internal
 */
class FileFinder
{
    /**
     * @param string $folder
     * @return Generator<string>
     */
    public static function findPhtmlFiles(string $folder): Generator
    {
        $pattern = '#^.*\.(phtml|php)$#';

        if (!is_dir($folder)) {
            return;
        }

        $foldersToExclude = [
            'node_modules',
            'var',
            'vendor',
        ];

        foreach ($foldersToExclude as $folderToExclude) {
            if (realpath($folder) === Path::getRootFolder() . DIRECTORY_SEPARATOR . $folderToExclude) {
                return;
            }
        }

        $finder = new Finder();
        $finder
            ->files()
            ->in($folder)
            ->name($pattern)
            ->exclude($foldersToExclude)
        ;

        foreach ($finder as $file) {
            yield $file->getPathname();
        }
    }
}
