<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole;

use DI\Container;
use Exception;
use Kiwa\Path;
use Nette\Loaders\RobotLoader;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Command\Command;

/**
 * Loads all commands that can be found to the console.
 *
 * @internal
 */
class DynamicCommandLoader
{
    /**
     * @var string[]
     */
    private array $commandPaths = [];
    
    /**
     * @var array<int, class-string>
     */
    private array $classesToExclude = [];

    /**
     * DynamicCommandLoader constructor.
     *
     * @param Application $application
     */
    public function __construct(
        private readonly Application $application,
    ) {
        $this
            ->setCommandPaths([
                Path::getRootFolder() . DIRECTORY_SEPARATOR . 'src',
                Path::getRootFolder() . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'bitandblack',
                Path::getRootFolder() . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'kiwa',
            ])
        ;
    }

    /**
     * Loads the commands.
     */
    public function loadCommands(): void
    {
        /** @var array<int, string> $classesWithCommand */
        $classesWithCommand = [];

        $robotLoader = new RobotLoader();
        
        foreach ($this->commandPaths as $commandPath) {
            if (file_exists($commandPath)) {
                $robotLoader->addDirectory($commandPath);
            }
        }
        
        $robotLoader->rebuild();
        $classesIndexed = $robotLoader->getIndexedClasses();

        foreach ($classesIndexed as $classString => $classFileName) {
            $isToExclude = in_array($classString, $this->classesToExclude, true);
            $isFromSymfony = str_starts_with($classString, 'Symfony\Component');

            if ($isToExclude || $isFromSymfony) {
                unset($classesIndexed[$classString]);
            }
        }

        /**
         * @var class-string $classString
         */
        foreach ($classesIndexed as $classString => $classFileName) {
            try {
                $reflected = new ReflectionClass($classString);
            }
            /** @phpstan-ignore-next-line */
            catch (ReflectionException) {
                continue;
            }

            if ($reflected->isInterface()) {
                continue;
            }
            
            if (is_a($classString, Command::class, true)) {
                $classesWithCommand[] = $classString;
            }
        }
        
        $container = new Container();
        
        foreach ($classesWithCommand as $classWithCommand) {
            try {
                /** @var Command $commandObject */
                $commandObject = $container->get($classWithCommand);
                $this->application->add($commandObject);
            } catch (Exception) {
                trigger_error('Cannot add command "' . $classWithCommand . '" to console.', E_USER_NOTICE);
            }
        }
    }

    /**
     * @return string[]
     */
    public function getCommandPaths(): array
    {
        return $this->commandPaths;
    }

    /**
     * @param string[] $commandPaths
     * @return DynamicCommandLoader
     */
    public function setCommandPaths(array $commandPaths): self
    {
        $this->commandPaths = $commandPaths;
        return $this;
    }

    /**
     * @return array<int, class-string>
     */
    public function getClassesToExclude(): array
    {
        return $this->classesToExclude;
    }

    /**
     * @param array<int, class-string> $classesToExclude
     * @return DynamicCommandLoader
     */
    public function setClassesToExclude(array $classesToExclude): self
    {
        $this->classesToExclude = $classesToExclude;
        return $this;
    }
}
