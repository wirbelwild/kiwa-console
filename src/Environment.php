<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole;

/**
 * Class Environment.
 */
class Environment
{
    private static bool $runsFromTemplate = false;

    /**
     * Sets if the CLI runs from a Kiwa Template.
     */
    public static function runsFromTemplate(): void
    {
        self::$runsFromTemplate = true;
    }

    /**
     * @return bool
     */
    public static function hasBeenCalledFromTemplate(): bool
    {
        return self::$runsFromTemplate;
    }
}
