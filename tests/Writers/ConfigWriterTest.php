<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Tests\Writers;

use Kiwa\Config\Generator\CacheControlGenerator;
use Kiwa\Config\Generator\ConfigGenerator;
use Kiwa\Config\Generator\PageGenerator;
use Kiwa\Exception\Config\MissingValueException;
use Kiwa\Path;
use Kiwa\TemplateConsole\Writers\ConfigWriter;
use PHPUnit\Framework\TestCase;
use ReflectionException;

/**
 * Class ConfigWriterTest
 *
 * @package Kiwa\TemplateConsole\Tests\Writers
 */
class ConfigWriterTest extends TestCase
{
    /**
     * Tests if pages can be detected correctly.
     */
    public function testCanGetPagesLevel1(): void
    {
        $config = [
            'pages' => [
                'de' => [
                    [
                        'name' => 'Page 1',
                    ],
                    [
                        'name' => 'Page 2',
                        'childOf' => false,
                    ],
                    [
                        'name' => 'Page 3',
                        'childOf' => 'Page 1',
                    ],
                ],
            ],
        ];
        
        $configWriter = $this->getMockBuilder(ConfigWriter::class)
            ->onlyMethods(['getConfig'])
            ->getMock()
        ;
        
        $configWriter
            ->method('getConfig')
            ->willReturn($config)
        ;
        
        self::assertEquals(
            $config,
            $configWriter->getConfig()
        );

        self::assertSame(
            [
                'Page 1',
                'Page 2',
            ],
            $configWriter->getPagesLevel1()
        );
    }

    /**
     * @throws MissingValueException
     * @throws ReflectionException
     */
    public function testCanCreatePHPMethods(): void
    {
        $config = new ConfigGenerator();
        $config
            ->setMainURL('url')
            ->addPage(
                PageGenerator::create()
                ->setLanguageCode('de')
                ->setName('name')
                ->setFile('file')
                ->enableCaching(60)
            )
            ->setCacheControl(
                CacheControlGenerator::create()
                ->setMaxAgeUnknownFiles(123)
            )
        ;
        
        self::assertSame(
            [
                'htaccess' => [
                    'useWWW' => false,
                    'useSSL' => false,
                ],
                'usePageSuffix' => 'html',
                'languageHandling' => 'strict',
                'urlStructure' => [
                    0 => 'name',
                    1 => 'subname',
                ],
                'mainURL' => 'url',
                'baseMetaTags' => [
                    'og:url' => 'url',
                ],
                'pages' => [
                    'de' => [
                        0 => [
                            'languageCode' => 'de',
                            'name' => 'name',
                            'getFile' => 'file',
                            'enableCache' => true,
                            'cacheLifetime' => 60,
                        ],
                    ],
                ],
                'cacheControl' => [
                    'maxAgeAssetFiles' => 5_184_000,
                    'maxAgeImmutableFiles' => 31_536_000,
                    'maxAgeUnknownFiles' => 123,
                ],
            ],
            $config->getConfig()
        );
        
        file_put_contents(
            Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php',
            '<?php return ' . var_export($config->getConfig(), true) . ';'
        );
        
        $configWriter = new ConfigWriter();
        $configWriter
            ->setOutputFormat(ConfigWriter::FORMAT_PHP_METHOD)
            ->rewriteConfig()
        ;
        
        $configPHPMethod = (string) file_get_contents(Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php');

        self::assertStringContainsString(
            '->addPage(PageGenerator::create()',
            $configPHPMethod
        );

        self::assertStringContainsString(
            '->setCacheControl(CacheControlGenerator::create()',
            $configPHPMethod
        );
        
        self::assertStringContainsString(
            '->enableCaching(60)',
            $configPHPMethod
        );
        
        unlink(Path::getConfigFolder() . DIRECTORY_SEPARATOR . 'config.php');
    }
}
