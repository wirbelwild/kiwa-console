<?php

namespace Kiwa\TemplateConsole\Tests\Commands;

use Kiwa\Path;
use Kiwa\TemplateConsole\Commands\TranslationClearCommand;
use Kiwa\TemplateConsole\Writers\TranslationWriter;
use PHPUnit\Framework\TestCase;
use Places2Be\Locales\LanguageCode;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class TranslationClearCommandTest.
 *
 * @package Kiwa\TemplateConsole\Tests\Commands
 */
class TranslationClearCommandTest extends TestCase
{
    public function testCanClear(): void
    {
        $translationWriter = new TranslationWriter();

        $testTranslationLanguageCode = (string) new LanguageCode('de-de');
        $testTranslationContent = [
            'My translation' => 'Meine Übersetzung',
            'My unneeded translation' => 'My unneeded translation',
        ];

        $translationWriter->saveTranslations([
            $testTranslationLanguageCode => $testTranslationContent,
        ]);
        
        $translationFile = Path::getLanguageFolder() . DIRECTORY_SEPARATOR . $testTranslationLanguageCode . '.php';
        
        $savedTranslationContent = include $translationFile;
        
        self::assertSame(
            $testTranslationContent,
            $savedTranslationContent
        );
        
        $translationClearCommand = new TranslationClearCommand($translationWriter);
        $commandTester = new CommandTester($translationClearCommand);
        
        $commandTester->execute([]);
        $display = $commandTester->getDisplay();
        
        self::assertStringContainsString(
            'Successfully',
            $display
        );

        $savedTranslationContent = include $translationFile;

        self::assertArrayNotHasKey(
            'My unneeded translation',
            $savedTranslationContent
        );
        
        unlink($translationFile);
    }
}
