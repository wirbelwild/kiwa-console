<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Tests\Commands;

use BitAndBlack\Helpers\FileSystemHelper;
use Kiwa\Path;
use Kiwa\TemplateConsole\Commands\TranslationUnusedRemoveCommand;
use Kiwa\TemplateConsole\Writers\TranslationWriter;
use PHPUnit\Framework\TestCase;
use Places2Be\Locales\LanguageCode;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Tester\CommandTester;

class TranslationUnusedRemoveCommandTest extends TestCase
{
    public function testCanFindUnusedTranslations(): void
    {
        $testFile1Name = 'test1.phtml';
        $testFile1Content = <<<PHP
        <?php 
        
        use Kiwa\Language\Text;
        
        ?>
        <section>
            <p>Hello world!</p>
            <p><?php echo new Text('My first translation'); ?></p>
            <p><?php echo new Text('My second translation'); ?></p>
            <p><?php echo new \Kiwa\Language\Text('My third translation'); ?></p>
            <?php echo '<p>' . new Text('My fourth translation') . '</p>'; ?>
        </section>
        PHP;

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $testFile1Name,
            $testFile1Content
        );

        $testFile2Name = 'test2.phtml';
        $testFile2Content = <<<PHP
        <?php 
        
        use Kiwa\Language\Text;
        
        \$translation = new Text('My fifth translation');
        
        ?>
        <section>
            <?php echo '<p>' . \$translation . '</p>'; ?>
        </section>
        PHP;

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $testFile2Name,
            $testFile2Content
        );

        $testTranslationLanguageCode1 = (string) new LanguageCode('de-de');
        $testTranslationContent1 = [
            'My second translation' => 'Meine zweite Übersetzung',
            'My unneeded translation' => 'Meine nicht verwendete Übersetzung',
        ];

        $testTranslationLanguageCode2 = (string) new LanguageCode('fr-fr');
        $testTranslationContent2 = [
            'My second translation' => 'Ma deuxième traduction',
            'My unneeded translation' => 'Ma traduction inutile',
        ];

        $translationWriter = new TranslationWriter();
        $translationWriter->saveTranslations([
            $testTranslationLanguageCode1 => $testTranslationContent1,
            $testTranslationLanguageCode2 => $testTranslationContent2,
        ]);
        
        $helperSet = new HelperSet([
            new QuestionHelper(),
        ]);

        $translationUnusedRemoveCommand = new TranslationUnusedRemoveCommand($translationWriter);
        $translationUnusedRemoveCommand->setHelperSet($helperSet);
        
        $commandTester = new CommandTester($translationUnusedRemoveCommand);
        
        $commandTester->execute([]);
        $display = $commandTester->getDisplay();

        self::assertStringContainsString(
            'Successfully',
            $display
        );

        self::assertStringContainsString(
            '2 translations have been found',
            $display
        );

        FileSystemHelper::deleteFolder(Path::getHTMLFolder());
        FileSystemHelper::deleteFolder(Path::getLanguageFolder());
    }
}
