<?php

namespace Kiwa\TemplateConsole\Tests\Commands;

use Kiwa\TemplateConsole\Commands\ComposerJsonUpdateCommand;
use PHPUnit\Framework\TestCase;
use ReflectionException;
use ReflectionMethod;

class ComposerJsonUpdateCommandTest extends TestCase
{
    /**
     * @throws ReflectionException
     */
    public function testCanMergeConfig(): void
    {
        $configExisting = [
            'name' => 'my-vendor/my-library',
            'authors' => [
                [
                    'name' => 'Äöüß',
                ],
            ],
            'require' => [
                'php' => '>=7.4',
            ],
            'scripts' => [
                'post-install-cmd' => [
                    '@my-custom-script',
                ],
                'post-package-install' => 'Kiwa\\ComposerHandler::oldValue',
            ],
        ];

        $configNew = [
            'scripts' => [
                'post-install-cmd' => [
                    '@kiwa-config-update',
                    '@kiwa-introduction',
                ],
                'post-package-install' => 'Kiwa\\ComposerHandler::newValue',
            ],
        ];

        $configExpected = [
            'name' => 'my-vendor/my-library',
            'authors' => [
                [
                    'name' => 'Äöüß',
                ],
            ],
            'require' => [
                'php' => '>=7.4',
            ],
            'scripts' => [
                'post-install-cmd' => [
                    '@my-custom-script',
                    '@kiwa-config-update',
                    '@kiwa-introduction',
                ],
                'post-package-install' => 'Kiwa\\ComposerHandler::newValue',
            ],
        ];
        
        $method = new ReflectionMethod(
            ComposerJsonUpdateCommand::class,
            'mergeConfiguration'
        );

        $method->setAccessible(true);

        $response = $method->invoke(new ComposerJsonUpdateCommand(), $configExisting, $configNew);

        $this->assertSame(
            $configExpected,
            $response
        );
    }
}
