<?php

/**
 * Kiwa. A feather-light web framework for professional static websites.
 *
 * @author Tobias Köngeter
 * @copyright Copyright © Bit&Black
 * @link https://www.kiwa.io
 * @license MIT
 */

namespace Kiwa\TemplateConsole\Tests\Commands;

use BitAndBlack\Helpers\FileSystemHelper;
use Kiwa\Path;
use Kiwa\TemplateConsole\Commands\TranslationFindMissingCommand;
use Kiwa\TemplateConsole\Writers\TranslationWriter;
use PHPUnit\Framework\TestCase;
use Places2Be\Locales\LanguageCode;
use Symfony\Component\Console\Helper\HelperSet;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Class TranslationFindMissingCommandTest.
 *
 * @package Kiwa\TemplateConsole\Tests\Commands
 */
class TranslationFindMissingCommandTest extends TestCase
{
    protected function tearDown(): void
    {
        FileSystemHelper::deleteFolder(Path::getHTMLFolder());
        FileSystemHelper::deleteFolder(Path::getLanguageFolder());
    }

    public function testCanFindMissingTranslation(): void
    {
        $testFile1Name = 'test1.phtml';
        $testFile1Content = <<<PHP
        <?php 
        
        use Kiwa\Language\Text;
        
        ?>
        <section>
            <p>Hello world!</p>
            <p><?php echo new Text('My first translation'); ?></p>
            <p><?php echo new Text('My second translation'); ?></p>
            <p><?php echo new \Kiwa\Language\Text('My third translation'); ?></p>
            <?php echo '<p>' . new Text('My fourth translation') . '</p>'; ?>
        </section>
        PHP;

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $testFile1Name,
            $testFile1Content
        );

        $testFile2Name = 'test2.phtml';
        $testFile2Content = <<<PHP
        <?php 
        
        use Kiwa\Language\Text;
        
        \$translation = new Text('My fifth translation');
        
        ?>
        <section>
            <?php echo '<p>' . \$translation . '</p>'; ?>
        </section>
        PHP;

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $testFile2Name,
            $testFile2Content
        );

        $testTranslationLanguageCode = (string) new LanguageCode('de-de');
        $testTranslationContent = [
            'My translation' => 'Meine Übersetzung',
            'My unneeded translation' => 'My unneeded translation',
        ];

        $translationWriter = new TranslationWriter();
        $translationWriter->saveTranslations([
            $testTranslationLanguageCode => $testTranslationContent,
        ]);
        
        $helperSet = new HelperSet([
            new QuestionHelper(),
        ]);

        $translationFindMissingCommand = new TranslationFindMissingCommand($translationWriter);
        $translationFindMissingCommand->setHelperSet($helperSet);
        
        $commandTester = new CommandTester($translationFindMissingCommand);
        
        $commandTester->execute([]);
        $display = $commandTester->getDisplay();

        self::assertStringContainsString(
            'Done.',
            $display
        );
    }

    public function testCanHandleMessageDomains(): void
    {
        $testFile1Name = 'test1.phtml';
        $testFile1Content = <<<PHP
        <?php 
        
        use Kiwa\Language\Text;
        
        ?>
        <section>
            <p>Hello world!</p>
            <p><?php echo new Text('My first translation', [], 'domain1'); ?></p>
            <p><?php echo new Text('My second translation'); ?></p>
            <p><?php echo new Text('My third translation', [], 'domain1'); ?></p>
            <p><?php echo new Text('My fourth translation'); ?></p>
        </section>
        PHP;

        file_put_contents(
            Path::getHTMLFolder() . DIRECTORY_SEPARATOR . $testFile1Name,
            $testFile1Content
        );

        $translations = [];
        
        $testTranslationMessageDomain1 = 'domain1.' . new LanguageCode('de-de');
        $testTranslationContent = [
            'My first translation' => 'Meine erste Übersetzung',
        ];

        $translations[$testTranslationMessageDomain1] = $testTranslationContent;

        $testTranslationMessageDomain2 = (string) new LanguageCode('de-de');
        $testTranslationContent = [
            'My second translation' => 'Meine zweite Übersetzung',
            'My fourth translation' => 'Meine vierte Übersetzung',
        ];

        $translations[$testTranslationMessageDomain2] = $testTranslationContent;

        $testTranslationMessageDomain3 = (string) new LanguageCode('en-gb');
        $testTranslationContent = [];

        $translations[$testTranslationMessageDomain3] = $testTranslationContent;

        $translationWriter = new TranslationWriter();
        $translationWriter->saveTranslations($translations);

        $helperSet = new HelperSet([
            new QuestionHelper(),
        ]);

        $translationFindMissingCommand = new TranslationFindMissingCommand($translationWriter);
        $translationFindMissingCommand->setHelperSet($helperSet);

        $commandTester = new CommandTester($translationFindMissingCommand);

        $commandTester->execute([]);
        $display = $commandTester->getDisplay();

        $display = (string) preg_replace('/\s+/', ' ', $display);

        self::assertStringContainsString(
            'My third translation │ domain1.de-de',
            $display
        );

        self::assertStringContainsString(
            'My second translation │ en-gb',
            $display
        );

        self::assertStringContainsString(
            'My fourth translation │ en-gb',
            $display
        );
    }
}
