<?php

namespace Kiwa\TemplateConsole\Tests;

use Kiwa\Language\Text;
use Kiwa\TemplateConsole\NodeVisitor;
use PhpParser\NodeTraverser;
use PhpParser\NodeVisitor\NameResolver;
use PhpParser\ParserFactory;
use PhpParser\PhpVersion;
use PHPUnit\Framework\TestCase;

class NodeVisitorTest extends TestCase
{
    public function testCanParse(): void
    {
        $content = '<?php 

use Kiwa\Language\Text;

$textVariable1 = new Text(\'My fifth translation\');
$textVariable2 = \'My sixth translation\';

?>
<section>
    <p>Hello world!</p>
    <p><?php echo new Text(\'My first translation\'); ?></p>
    <p><?php echo new Text(\'My second translation\'); ?></p>
    <p><?php echo new \Kiwa\Language\Text(\'My third translation\', [], \'domain2\'); ?></p>
    <?php echo \'<p>\' . new Text(\'My fourth translation\') . \'</p>\'; ?>
    <?php echo \'<p>\' . $textVariable1 . \'</p>\'; ?>
    <p><?php echo new Text($textVariable2); ?></p>
</section>
';
        
        $parserFactory = new ParserFactory();
        $parser = $parserFactory->createForVersion(PhpVersion::getHostVersion());
        $traverser = new NodeTraverser();
        $visitor = new NodeVisitor([
            Text::class,
        ]);
        
        $stmts = $parser->parse($content);
        $traverser->addVisitor(new NameResolver());
        $traverser->addVisitor($visitor);
        
        self::assertNotNull($stmts);

        $traverser->traverse($stmts);
        
        $translationTextValues = $visitor->getConstructorArguments();

        /**
         * As the parser doesn't handle variables, we ignore one case.
         */
        self::assertCount(
            5,
            $translationTextValues
        );

        foreach ($translationTextValues as $translationTextValue) {
            self::assertArrayHasKey(
                'text',
                $translationTextValue['arguments']
            );

            self::assertIsString(
                $translationTextValue['arguments']['text']
            );

            self::assertStringNotContainsString(
                'My sixth translation',
                $translationTextValue['arguments']['text']
            );
        }

        self::assertArrayNotHasKey(
            'messageDomain',
            $translationTextValues[2]['arguments']
        );

        self::assertSame(
            'My second translation',
            $translationTextValues[2]['arguments']['text']
        );

        self::assertSame(
            'domain2',
            $translationTextValues[3]['arguments']['messageDomain']
        );
    }
}
