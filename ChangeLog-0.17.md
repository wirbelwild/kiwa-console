# Changes in Kiwa Console v0.17

## 0.17.3 2024-09-12

### Fixed

-   Remove deprecated encoding.

## 0.17.2 2023-08-28

### Changed

-   The `translation:find-missing` command searches in path `/src` now too and looks for `php` and `phtml` files. There is also the possibility to search in a given path.

## 0.17.0 2022-01-11

### Added

-   Added `symfony/dotenv` to load environment variables.
-   Added possibility to handle message domains when translating texts.