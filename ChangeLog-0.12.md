# Changes in Kiwa Console.

## 0.12.0 2020-11-26 

### Added 

-   The new `server:start` command allows to run a PHP server on a local machine.
-   Added support for PHP 8.