# Changes in Kiwa Console v0.19

## 0.19.3 2023-08-28

### Changed

-   The `translation:find-missing` command searches in path `/src` now too and looks for `php` and `phtml` files. There is also the possibility to search in a given path.

## 0.19.0 2022-04-13

### Fixed

-   Fixed missing method in PHP < 8.0

### Added 

-   Added the `ComposerJsonUpdateCommand` that allows updating the `composer.json` file.
-   Added `bitandblack` as allowed vendor to the dynamic command loader. Added setter methods too.