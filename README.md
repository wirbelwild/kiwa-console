[![PHP from Packagist](https://img.shields.io/packagist/php-v/kiwa/console)](http://www.php.net)
[![Latest Stable Version](https://poser.pugx.org/kiwa/console/v/stable)](https://packagist.org/packages/kiwa/console)
[![Total Downloads](https://poser.pugx.org/kiwa/console/downloads)](https://packagist.org/packages/kiwa/console)
[![License](https://poser.pugx.org/kiwa/console/license)](https://packagist.org/packages/kiwa/console)

<p align="center">
    <a href="https://www.kiwa.io" target="_blank">
        <img src="https://www.kiwa.io/build/images/kiwa-logo.png" alt="Kiwa Logo" width="150">
    </a>
</p>

# Kiwa Console 

This library provides a CLI to help handling a website made with [Kiwa](https://www.kiwa.io).

## Installation 

This library is made for the use with [Composer](https://packagist.org/packages/kiwa/console). Add it to your project by running `$ composer require kiwa/console`. 

__Please note__: The easiest way to create a website with Kiwa is by using the [Website Skeleton](https://bitbucket.org/wirbelwild/kiwa-website-skeleton). It will create all the needed files and loads the `kiwa/core` along with the `kiwa/console` as dependencies.

## Available Commands

| Command                                                                               | Description                                                      |
|---------------------------------------------------------------------------------------|------------------------------------------------------------------|
| [`asset:attribute-cache:create`](./src/Commands/AssetAttributeCacheCreateCommand.php) | Creates a asset attribute cache.                                 |
| [`assets:install`](./src/Commands/InstallAssetsCommand.php)                           | Installs all asset files.                                        |
| [`basic:metatags`](./src/Commands/BasicMetaTagsCommand.php)                           | Defines basic meta tags.                                         |
| [`cache:clear`](./src/Commands/CacheClearCommand.php)                                 | Deletes all cache files.                                         |
| [`composer:json:update`](./src/Commands/ComposerJsonUpdateCommand.php)                | Updates the composer.json file.                                  |
| [`config:convert`](./src/Commands/ConfigConvertCommand.php)                           | Converts the config file into another format.                    |
| [`config:validate`](./src/Commands/ConfigValidateCommand.php)                         | Validates the config file.                                       |
| [`files:update`](./src/Commands/FilesUpdateCommand.php)                               | Updates all static files.                                        |
| [`htaccess:create`](./src/Commands/HtaccessCreateCommand.php)                         | Creates a .htaccess file.                                        |
| [`introduction`](./src/Commands/IntroductionCommand.php)                              | Shows the introduction.                                          |
| [`language:define`](./src/Commands/LanguageHandlingCommand.php)                       | Defines the language handling.                                   |
| [`metastorm:file:create`](./src/Commands/MetaStormFileCreateCommand.php)              | Creates or updates a `.meta-storm.xml` file in the project root. |
| [`page:create`](./src/Commands/PageCreateCommand.php)                                 | Creates a new page.                                              |
| [`robotstxt:create`](./src/Commands/RobotsTxtCreateCommand.php)                       | Creates a robots.txt file.                                       |
| [`server:start`](./src/Commands/ServerStartCommand.php)                               | Starts a local PHP server.                                       |
| [`translation:clear`](./src/Commands/TranslationClearCommand.php)                     | Clears all translation files.                                    |
| [`translation:find-missing`](./src/Commands/TranslationFindMissingCommand.php)        | Finds missing translations.                                      |
| [`translation:sort`](./src/Commands/TranslationSortCommand.php)                       | Sorts all translations.                                          |
| [`translation:unused:remove`](./src/Commands/TranslationUnusedRemoveCommand.php)      | Finds unused translations in the language files.                 |
| [`url:define`](./src/Commands/PathStructureCommand.php)                               | Defines the url structure.                                       |

## Help 

If you have any questions, feel free to contact us under `hello@bitandblack.com`.

Further information about Bit&Black can be found under [www.bitandblack.com](https://www.bitandblack.com).