# Changes in Kiwa Console v1.0

## 1.0.0 2024-12-17

### Changed

-   PHP >= 8.2 is now required.
-   All previously deprecated classes and methods have been removed.