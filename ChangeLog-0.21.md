# Changes in Kiwa Console v0.21

## 0.21.2 2023-08-28

### Changed

-   The `translation:find-missing` command searches in path `/src` now too and looks for `php` and `phtml` files. There is also the possibility to search in a given path.

## 0.21.1 2023-07-28

### Fix

-   Fix message domain handling in the `TranslationUnusedRemoveCommand`.

## 0.21.0 2023-07-25

### Added

-   Added the `TranslationUnusedRemoveCommand` to remove unused translations.