# Changes in Kiwa Console v0.25

## 0.25.0 2025-10-01

### Changed

-   Deprecated command `config:update` in favor of `htaccess:create`.