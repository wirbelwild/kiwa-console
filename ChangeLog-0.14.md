# Changes in Kiwa Console 0.14

## 0.14.0 2021-03-23

### Changed

-   Makes use of `kiwa/core` `^0.24`.
-   Removes the use of `roave/betterreflection`.
-   Ready for PHP 8.